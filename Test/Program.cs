﻿using DAL;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
      public delegate int print(int a,int b);
      static  private print prints;
        static void Main(string[] args)
        {
            #region
            //PlcController plc1 = new PlcController("192.168.0.10",502,PlcType.西门子);
            //PlcController plc2 = new PlcController("192.168.0.2",503, PlcType.西门子);
            //Plcservice plcservice1 = new Plcservice(plc1);
            //Plcservice plcservice2 = new Plcservice(plc2);
            //plcservice1.OpenConnect();
            //PlcVarValue plcVar = new PlcVarValue();
            //plcVar.VarName = "dd";
            //plcVar.VarAddress = "vw100";
            //plc1.plcVarValues.Add(plcVar);

            //if (plcservice1.IsConnected)
            //{
            //    Console.WriteLine("连接成功");
            //}
            //string b = plcservice1.IsConnected ? "1" : "2";
            //Console.WriteLine(b);
            //Console.WriteLine($"plc1 ip is{plcservice1.IP}          plc2 ip is{plcservice2.IP}     ");
            //int aa = 13253522;
            //float bb = 3.145f;
            //plcservice1.WriteIntSingleRegisters(1,10,aa);
            //plcservice1.WriteFloatSingleRegisters(1, 12, bb);
            //Action<string> action = new Action<string>((s) => { Console.WriteLine(s); });
            //action.Invoke("aa");
            //prints += Add;
            //int a = prints(1, 2);
            #endregion

            List<AlarmVar> alarmVars = new List<AlarmVar>();
            for (int i = 0; i < 10; i++)
            {
                alarmVars.Add(new AlarmVar() {alarmInfo=i.ToString(), ocreenceTime = DateTime.Now });
                Thread.Sleep(1000);
            }
            Console.WriteLine("排序前");
            foreach (var item in alarmVars)
            { 
                Console.WriteLine(item.alarmInfo+":"+item.ocreenceTime.ToString());
            }

            alarmVars.Sort();
            Console.WriteLine("排序后");
            foreach (var item in alarmVars)
            {
                Console.WriteLine(item.alarmInfo + ":" + item.ocreenceTime.ToString());
            }




            Console.ReadKey();
        }

        private static int Add(int a,int b)
        { return a + b; }
        
        
    }
    public class AlarmVar : IComparable
    {
        public bool regvalue { get; set; }

        public string alarmInfo { get; set; }

        public DateTime ocreenceTime { get; set; }

        public int CompareTo(object obj)
        {
            if (this.ocreenceTime > ((AlarmVar)obj).ocreenceTime)
            {
                return 1;
            }
            else if ((this.ocreenceTime == ((AlarmVar)obj).ocreenceTime))
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
    }
    //自定义排序类别
    public class CardCompare : IComparer<AlarmVar>
    {
        //实现的是降序排列，你也可以改变返回值实现升序排列


        public int Compare(AlarmVar x, AlarmVar y)
        {
            if (x.ocreenceTime > y.ocreenceTime)
            {
                return 1;
            }
            else if (x.ocreenceTime < y.ocreenceTime)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }
    }
