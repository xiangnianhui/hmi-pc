﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DAL
{
   /// <summary>
   /// 序列化类，用于对文件的序列化和反序列化
   /// </summary>
   /// <typeparam name="T">泛型类</typeparam>
 public   static class SerializeHelper<T>
    {
        //-------------------------------XML------------------------------------------
       /// <summary>
       /// xml序列化写入
       /// </summary>
       /// <param name="path">写入地址</param>
       /// <param name="list">类型的集合</param>
        public static void XmlSerializeWriter(string path, List<T> list)
        {
            if (list==null)
            {
                return;
            }
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
            Stream fs = null;
            XmlWriter writer = null;
        
            try
            {
                // Create an XmlTextWriter using a FileStream.
                fs = new FileStream(path, FileMode.Create, System.IO.FileAccess.ReadWrite, System.IO.FileShare.ReadWrite);
                writer = new XmlTextWriter(fs, Encoding.Unicode);
                // Serialize using the XmlTextWriter.
                serializer.Serialize(writer, list);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (writer!=null)
                {
                    writer.Close();
                }
                if (fs!=null)
                {
                    fs.Close();
                }  
            }  
        }

        /// <summary>
        ///xml 反序列化读取
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<T> XmlSerializeRead(string path)
        {
            List<T> list = new List<T>();
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
            // Serialize using the XmlTextWriter.
            try
            {
                if (File.Exists(path))
                {
                    using (Stream reader = new FileStream(path, FileMode.Open, System.IO.FileAccess.ReadWrite, System.IO.FileShare.ReadWrite))

                    {

                        list = (List<T>)serializer.Deserialize(reader);
                    }
                }
                else
                {
                  LogHelper.AddLog("path错误");
                }
            }
            catch (Exception)
            {
                throw;
            }
            return list;
        }



        //-------------------------------------Binary------------------------------------
        /// <summary>
        /// 二值化序列化写入
        /// </summary>
        /// <param name="path"></param>
        /// <param name="list"></param>
        public static void BinarySerialzeWrite(string path, List<T> list)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = null;
            try
            {
                stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
                formatter.Serialize(stream, list);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }

        /// <summary>
        /// 二值化反序列读取
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<T> BinarySerialzeRead(string path)
        {
            List<T> list = null;
            IFormatter formatter = new BinaryFormatter();
            Stream stream = null;
            if (File.Exists(path))
            {
                try
                {
                    stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                    list = (List<T>)formatter.Deserialize(stream);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Close();
                    }
                }
            }
            return list;
        }
    }
}
