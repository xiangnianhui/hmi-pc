﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    //PLC报警控制
 public   class PlcAlarmService
    {

        #region
        /// <summary>
        /// 报警地址
        /// </summary>
        public static string AlarmListPath = "AlarmRecipe\\AlarmList.xml";
        //报警数量
        public static int AlarmNum = 0;

        #endregion

        #region  方法
        /// <summary>
        /// 检查线圈状态返回发生报警的list
        /// </summary>
        /// <returns></returns>
        public static List<PlcAlarmCoil> CheckCoilState(ref List<PlcAlarmCoil> plcAlarmCoils)
        {
            string timestring = "0002/1/1 0:00:00";
            //把这个字符串转化为时间
            DateTime InitTime = Convert.ToDateTime(timestring);
            //发生报警的线圈list
            List<PlcAlarmCoil> OccurrentList = new List<PlcAlarmCoil>();         
            foreach (PlcAlarmCoil coil in plcAlarmCoils)
            {
                switch (coil.Vartype)
                {
                    case PlcDatatype.Bool:
                        if (Convert.ToBoolean(coil.VarValue))
                        {
                            if (coil.OccurrentTime <= InitTime)
                            {
                                coil.OccurrentTime = DateTime.Now;
                            }
                            AlarmNum++;
                            OccurrentList.Add(coil);
                        }
                        else
                        {
                            coil.OccurrentTime = InitTime;
                        }
                        break;
                    case PlcDatatype.Short:
                        break;
                    case PlcDatatype.Ushort:
                        break;
                    case PlcDatatype.Int:
                        break;
                    case PlcDatatype.Uint:
                        break;
                    case PlcDatatype.Float:
                        break;
                    case PlcDatatype.Double:
                        break;
                    default:
                        break;
                }
            }
            //发生报警的线圈list排序
            List<PlcAlarmCoil> OccurrentListOrder = new List<PlcAlarmCoil>();
            foreach (PlcAlarmCoil item in OccurrentList)
            {
                OccurrentListOrder.Add(item);
            }
            OccurrentListOrder.Sort();//按照时间降序

            return OccurrentListOrder;
        }


        #endregion
    }
}
