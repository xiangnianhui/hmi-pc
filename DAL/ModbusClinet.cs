﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using NModbus;
using NModbus.Data;


namespace DAL
{

    public abstract class ModbusClient
    { 
        //modbus工程
        private ModbusFactory modbusFactory;
        //控制模块
        private IModbusMaster master;
        //tcp连接通道
        private TcpClient tcpClient;
        //连接ip
        public string IP { get; set; }
        //连接端口号
        public int Port { get; set; }
        /// <summary>
        /// 是否连接
        /// </summary>
        public bool IsConnected
        {
            get
            {
                if (tcpClient != null)
                {
                   
                    return tcpClient.Connected;
                }
                return false;
            }
            set { }

        }

        
        //通讯连接
        public  bool OpenConnect()
        {
            try
            {
                modbusFactory = new ModbusFactory();
                tcpClient = new TcpClient(IP, Port);
                master = modbusFactory.CreateMaster(tcpClient);
                master.Transport.ReadTimeout = 2000;
                master.Transport.Retries = 10;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("连接出错 " + e.Message);
                return false;
               
            }
          
        }
        //通讯关闭
        public void DisConnecet()
        {
            //tcpClient.GetStream().Close();
            tcpClient.Close();
            IsConnected = false;
        }


        public bool[] ReadCoils(byte slaveAddress, ushort startAddress, ushort num)
        {
            return master.ReadCoils(slaveAddress, startAddress, num);
        }

        public bool[] ReadInputs(byte slaveAddress, ushort startAddress, ushort num)
        {
            return master.ReadInputs(slaveAddress, startAddress, num);
        }

        public ushort[] ReadHoldingRegisters(byte slaveAddress, ushort startAddress, ushort num)
        {
            return master.ReadHoldingRegisters(slaveAddress, startAddress, num);
        }

        public ushort[] ReadInputRegisters(byte slaveAddress, ushort startAddress, ushort num)
        {
            return master.ReadInputRegisters(slaveAddress, startAddress, num);
        }

        public void WriteSingleCoil(byte slaveAddress, ushort startAddress, bool value)
        {
            master.WriteSingleCoil(slaveAddress, startAddress, value);
        }

        public void WriteSingleRegister(byte slaveAddress, ushort startAddress, ushort value)
        {
            master.WriteSingleRegister(slaveAddress, startAddress, value);
        }

        public void WriteMultipleCoils(byte slaveAddress, ushort startAddress, bool[] value)
        {
            master.WriteMultipleCoils(slaveAddress, startAddress, value);
        }

        public void WriteMultipleRegisters(byte slaveAddress, ushort startAddress, ushort[] value)
        {
            master.WriteMultipleRegisters(slaveAddress, startAddress, value);
        }

        

        /// <summary>
        /// 复归型
        /// </summary>
        /// <param name="slaveAddress"></param>
        /// <param name="startAddress"></param>
        /// <param name="value"></param>
        public void SR(byte slaveAddress, ushort startAddress, ushort value)
        {
            master.WriteSingleRegister(slaveAddress, startAddress, value);

            master.WriteSingleRegister(slaveAddress, startAddress, 0);

        }



    }
}

