﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace DAL
{
  public  class FileHelper
    {
        /// <summary>
        /// 查找当前文件夹下的所有文件
        /// </summary>
        public static void FileFindAll(string filePathByForeach, ref string result)
        {
            DirectoryInfo theFolder = new DirectoryInfo(filePathByForeach);
            DirectoryInfo[] dirInfo = theFolder.GetDirectories();//获取所在目录的文件夹
            FileInfo[] file = theFolder.GetFiles();//获取所在目录的文件

            foreach (FileInfo fileItem in file) //遍历文件
            {
                //result += "dirName:" + fileItem.DirectoryName + "    fileName:" + fileItem.Name + "\n";
                result += fileItem.Name + "\n";
            }
            //遍历文件夹
            foreach (DirectoryInfo NextFolder in dirInfo)
            {
                FileFindAll(NextFolder.FullName, ref result);
            }
        }
        /// <summary>
        /// 文件删除
        /// </summary>
        /// <param name="path">路径</param>
        public static bool FileDel(string path)
        {
            try
            {
                File.Delete(path);
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
                
            }
           
           
        }


        
    }
}
