﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class DBHelper
    {
        /// <summary>
        ///保存数据连接字符串 
        /// </summary>
        private static string constr;
        /// <summary>
        /// 构造函数
        /// </summary>
        static DBHelper()
        {
            constr = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
        }
        /// <summary>
        /// 数据库增删改操作
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="sqlParameters"></param>
        /// <returns></returns>
        public static int Updata(string sql, SqlParameter[] sqlParameters)
        {
            using (SqlConnection conn = new SqlConnection(constr))
            {
                try
                {
                    //打开连接
                    conn.Open();
                    //新建sql命令
                    SqlCommand command = new SqlCommand(sql,conn);
                    //如果参数不为null，将参数赋值给命令的参数
                    if (sqlParameters != null)
                    {
                        command.Parameters.AddRange(sqlParameters);
                    }
                    return command.ExecuteNonQuery();


                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
        /// <summary>
        /// 通用数据库查询
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="sqlParameters"></param>
        /// <returns></returns>
        public static SqlDataReader Query(string sql, SqlParameter[] sqlParameters)
        {
            SqlConnection conn = new SqlConnection(constr);
           
                try
                {
                    //打开连接
                    conn.Open();
                    //新建sql命令
                    SqlCommand command = new SqlCommand(sql,conn);
                    //如果参数不为null，将参数赋值给命令的参数
                    if (sqlParameters != null)
                    {
                        command.Parameters.AddRange(sqlParameters);
                    }
                    return command.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (Exception)
                {

                    throw;
                }
            
        }
    }
}
