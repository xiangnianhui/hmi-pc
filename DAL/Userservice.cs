﻿using Models;
using System.Collections.Generic;


namespace DAL
{
    public class Userservice
    {

        //当前用户
        public static User Currentuser = null;
        //用户队列
        public  List<User> Users = new List<User>();
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user"></param>
        public void UserAdd(User user)
        {
            this.Users.Add(user);
        }
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="user"></param>
        public void UserDel(User user)
        {
            this.Users.Remove(user);
        }


        /// <summary>
        ///  检查是否管理员
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool CheckAdmin()
        {
            foreach (var use in this.Users)
            {
                if (use.Name == "admin" && use.Author == Authority.SA)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 创建管理员
        /// </summary>
        /// <returns></returns>
        public User CreateAdmin()
        {
            return (new User("admin", "admin", Authority.SA));
        }

        /// <summary>
        /// 检查是否存在相同的用户
        /// </summary>
        /// <param name="user"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool CheckEqualUser( string name)
        {
            foreach (var use in this.Users)
            {
                if (use.Name == name)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 检查是否有存在这个用户，如果有的话 就返回这个用户
        /// </summary>
        /// <param name="name"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public User CheckExistUser(string name, string pwd)
        {
            User user = null;
            foreach (var use in Users)
            {
                if (use.Name==name&&use.PWD==pwd)
                {
                    user = use;
                    return user;
                }
            }
            return user;
        }

    }
}
