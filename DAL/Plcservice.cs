﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DAL
{
    public class Plcservice : ModbusClient
    {
        #region 构造方法
        public Plcservice(PlcController plc)
        {
            this.plcController = plc;
            IP = plc.Ip;
            Port = plc.Port;
        }
        #endregion

        #region 字段
        private bool isConnect = false;
        #endregion

        #region 属性
        /// <summary>
        /// plc控制器
        /// </summary>
        private PlcController plcController;
        //配方下载进度条委托
        public delegate void DownloadProcess(int currentvalue, int maxvalue);
        //配方下载进度条实例
        public DownloadProcess downloadprocess;
        //检查是否连接成功
        public bool IsConnect
        {
            get
            {
                NetCheck();
                return isConnect;
            }
        }

        #endregion

        #region  方法

        /// <summary>
        /// 32位有符号整数写入plc
        /// </summary>
        /// <param name="slaveAddress">设备站号</param>
        /// <param name="startAddress">写入起始地址</param>
        /// <param name="value">写入值</param>
        public void WriteIntSingleRegisters(byte slaveAddress, ushort startAddress, int value)
        {
            ushort[] uvalue = new ushort[2];
            uvalue = Data2Convert.Int2Ushort(value, plcController.bytebyte);
            WriteMultipleRegisters(slaveAddress, startAddress, uvalue);
        }
        /// <summary>
        /// 单精度浮点数写入plc
        /// </summary>
        /// <param name="slaveAddress">设备站号</param>
        /// <param name="startAddress">写入起始地址</param>
        /// <param name="value">写入值</param>
        public void WriteFloatSingleRegisters(byte slaveAddress, ushort startAddress, float value)
        {
            ushort[] uvalue = new ushort[2];
            uvalue = Data2Convert.Float2Ushort(value, plcController.bytebyte);
            WriteMultipleRegisters(slaveAddress, startAddress, uvalue);
        }
        //配方数据写入
        public void WriteRecipeData(List<PlcVarValue> plcVars)
        {
            int maxvalue = plcVars.Count;
            int processvalue = 0;
            foreach (PlcVarValue Var in plcVars)
            {
                ushort address;
                switch (Var.Vartype)
                {
                    case PlcDatatype.Bool:
                        break;
                    case PlcDatatype.Short:
                        break;
                    case PlcDatatype.Ushort:
                        if (ushort.TryParse(Var.VarAddress, out address))
                        {
                            WriteSingleRegister(1, Convert.ToUInt16(Var.VarAddress), Convert.ToUInt16(Var.VarValue));
                        }
                        break;
                    case PlcDatatype.Int:
                        if (ushort.TryParse(Var.VarAddress, out address))
                        {
                            WriteFloatSingleRegisters(1, Convert.ToUInt16(Var.VarAddress), Convert.ToSingle(Var.VarValue));
                        }
                        break;
                    case PlcDatatype.Uint:
                        break;
                    case PlcDatatype.Float:
                        if (ushort.TryParse(Var.VarAddress, out address))
                        {
                            WriteIntSingleRegisters(1, Convert.ToUInt16(Var.VarAddress), Convert.ToInt32(Var.VarValue));
                        }
                        break;
                    case PlcDatatype.Double:
                        break;
                    default:
                        break;
                }
                processvalue++;
                downloadprocess(processvalue, maxvalue);
                Thread.Sleep(10);
            }
        }
        //配方数据读取
        public void ReadRecipeData(List<PlcVarValue> plcVars)
        {
            foreach (PlcVarValue Var in plcVars)
            {
                switch (Var.Vartype)
                {
                    case PlcDatatype.Bool:
                        break;
                    case PlcDatatype.Short:
                        break;
                    case PlcDatatype.Ushort:
                        ushort address;
                        if (ushort.TryParse(Var.VarAddress, out address))
                        {
                            ReadHoldingRegisters(1, Convert.ToUInt16(Var.VarAddress), 1);
                        }
                        break;
                    case PlcDatatype.Int:
                        break;
                    case PlcDatatype.Uint:
                        break;
                    case PlcDatatype.Float:
                        break;
                    case PlcDatatype.Double:
                        break;
                    default:
                        break;
                }
            }
        }

        //检查网络连接情况
        private void NetCheck()
        {
            Ping ping = new System.Net.NetworkInformation.Ping();
            PingReply pingReply = ping.Send(System.Net.IPAddress.Parse(IP), 1000);//Ping百度，500毫秒超时
                                                                                 //判断ping返回来的结果
            if (pingReply.Status == System.Net.NetworkInformation.IPStatus.Success)
            {
                isConnect = true;
            }
            else
            {
                isConnect = false;
            }

        }

        #endregion

    }
}
