﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    
    /// <summary>
    /// 数据转换
    /// </summary>
    public class Data2Convert
    {
        /// <summary>
        /// 获取数组字节
        /// </summary>
        /// <param name="data">输入数组字节</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">转换长度-4的倍数</param>
        /// <param name="type">字节转换类型</param>
        /// <returns></returns>
        public static byte[] GetArrayByte(byte[] data, int startpos, int length, Bytebyte type)
        {
            byte[] result = new byte[length];
            if (data != null && data.Length >= length)
            {
                switch (type)
                {
                    case Bytebyte.ABCD:

                        result[0] = data[startpos];
                        result[1] = data[startpos + 1];
                        result[2] = data[startpos + 2];
                        result[3] = data[startpos + 3];

                        break;
                    case Bytebyte.CDAB:

                        result[2] = data[startpos];
                        result[3] = data[startpos + 1];
                        result[0] = data[startpos + 2];
                        result[1] = data[startpos + 3];

                        break;
                    case Bytebyte.BADC:

                        result[1] = data[startpos];
                        result[0] = data[startpos + 1];
                        result[3] = data[startpos + 2];
                        result[2] = data[startpos + 3];

                        break;
                    case Bytebyte.DCBA:

                        result[3] = data[startpos];
                        result[2] = data[startpos + 1];
                        result[1] = data[startpos + 2];
                        result[0] = data[startpos + 3];

                        break;
                    default:
                        break;
                }
            }
            return result;

        }


        /// <summary>
        /// 两个无符号16位整数转换为4个字节
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">长度</param>
        /// <param name="type">字节类型</param>
        /// <returns></returns>
        public static byte[] Ushort2byte(ushort[] data, int startpos, int length, Bytebyte type)
        {
            byte[] result = new byte[4];
            byte[] a = BitConverter.GetBytes(data[startpos]);
            byte[] b = BitConverter.GetBytes(data[startpos+1]);
            if (data != null && data.Length >= 2)
            {
                switch (type)
                {
                    case Bytebyte.ABCD:
                        result[0] = a[0];
                        result[1] = a[1];
                        result[2] = b[0];
                        result[3] = b[1];
                        break;
                    case Bytebyte.CDAB:

                        result[0] = b[0];
                        result[1] = b[1];
                        result[2] = a[0];
                        result[3] = a[1];

                        break;
                    case Bytebyte.BADC:

                        result[3] = b[0];
                        result[2] = b[1];
                        result[1] = a[0];
                        result[0] = a[1];

                        break;
                    case Bytebyte.DCBA:
                        result[0] = b[1];
                        result[1] = b[0];
                        result[2] = a[1];
                        result[3] = a[0];
                        

                        break;
                    default:
                        break;
                }
            }
            return result;

        }

        /// <summary>
        /// 无符号16位整数转换为32位整数
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">长度</param>
        /// <param name="type">字节类型</param>
        /// <returns></returns>
        public static int Ushort2Int(ushort[] data, int startpos, int length, Bytebyte type)
        {
            byte[] result = new byte[4];
            byte[] a = BitConverter.GetBytes(data[startpos]);
            byte[] b = BitConverter.GetBytes(data[startpos + 1]);
            if (data != null && data.Length >= 2)
            {
                switch (type)
                {
                    case Bytebyte.ABCD:
                        result[0] = a[0];
                        result[1] = a[1];
                        result[2] = b[0];
                        result[3] = b[1];
                        break;
                    case Bytebyte.CDAB:

                        result[0] = b[0];
                        result[1] = b[1];
                        result[2] = a[0];
                        result[3] = a[1];

                        break;
                    case Bytebyte.BADC:

                        result[3] = b[0];
                        result[2] = b[1];
                        result[1] = a[0];
                        result[0] = a[1];

                        break;
                    case Bytebyte.DCBA:
                        result[0] = b[1];
                        result[1] = b[0];
                        result[2] = a[1];
                        result[3] = a[0];


                        break;
                    default:
                        break;
                }
            }
            return BitConverter.ToInt32(result,0);
        }

        /// <summary>
        /// 无符号16位整数转换为单精度浮点数
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">长度</param>
        /// <param name="type">字节类型</param>
        /// <returns></returns>
        public static float Ushort2Float(ushort[] data, int startpos, int length, Bytebyte type)
        {
            byte[] result = new byte[4];
            byte[] a = BitConverter.GetBytes(data[startpos]);
            byte[] b = BitConverter.GetBytes(data[startpos + 1]);
            if (data != null && data.Length >= 2)
            {
                switch (type)
                {
                    case Bytebyte.ABCD:
                        result[0] = a[0];
                        result[1] = a[1];
                        result[2] = b[0];
                        result[3] = b[1];
                        break;
                    case Bytebyte.CDAB:

                        result[0] = b[0];
                        result[1] = b[1];
                        result[2] = a[0];
                        result[3] = a[1];

                        break;
                    case Bytebyte.BADC:
                        result[0] = a[1];
                        result[1] = a[0];
                        result[2] = b[1];
                        result[3] = b[0];
                      
                        
                      

                        break;
                    case Bytebyte.DCBA:
                        result[0] = b[1];
                        result[1] = b[0];
                        result[2] = a[1];
                        result[3] = a[0];


                        break;
                    default:
                        break;
                }
            }
            return BitConverter.ToSingle(result, 0);
        }


        /// <summary>
        ///uint32 转换为无符号16位整数
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">长度</param>
        /// <param name="type">字节类型</param>
        /// <returns></returns>
        public static ushort[] Uint2Ushort(uint data, Bytebyte type)
        {
            byte[] result = new byte[4];
            byte[] a = BitConverter.GetBytes(data);
            ushort[] result1 = new ushort[2];
            if (data != null )
            {
                switch (type)
                {
                    case Bytebyte.ABCD:
                        result[0] = a[0];
                        result[1] = a[1];
                        result[2] = a[2];
                        result[3] = a[3];
                        break;
                    case Bytebyte.CDAB:

                        result[0] = a[2]; ;
                        result[1] = a[3]; ;
                        result[2] = a[0];
                        result[3] = a[1];

                        break;
                    case Bytebyte.BADC:

                        result[0] = a[1];
                        result[1] = a[0];
                        result[2] = a[3]; 
                        result[3] = a[2];                                             

                        break;
                    case Bytebyte.DCBA:
                        result[0] = a[3];
                        result[1] = a[2];
                        result[2] = a[1];
                        result[3] = a[0];

                        break;
                    default:
                        break;
                }
            }
           result1[0]= BitConverter.ToUInt16(result, 0);
            result1[1] = BitConverter.ToUInt16(result, 2);
            return result1;
        }
        /// <summary>
        ///uint32[] 转换为无符号16位整数
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">长度</param>
        /// <param name="type">字节类型</param>
        /// <returns></returns>
        public static ushort[] Uint2Ushort(uint[] data,int startindex, Bytebyte type)
        {
            byte[] result = new byte[4];
            byte[] a = BitConverter.GetBytes(data[startindex]);
            ushort[] result1 = new ushort[2];
            if (data != null)
            {
                switch (type)
                {
                    case Bytebyte.ABCD:
                        result[0] = a[0];
                        result[1] = a[1];
                        result[2] = a[2];
                        result[3] = a[3];
                        break;
                    case Bytebyte.CDAB:

                        result[0] = a[2]; ;
                        result[1] = a[3]; ;
                        result[2] = a[0];
                        result[3] = a[1];

                        break;
                    case Bytebyte.BADC:

                        result[0] = a[1];
                        result[1] = a[0];
                        result[2] = a[3];
                        result[3] = a[2];




                        break;
                    case Bytebyte.DCBA:
                        result[0] = a[3];
                        result[1] = a[2];
                        result[2] = a[1];
                        result[3] = a[0];


                        break;
                    default:
                        break;
                }
            }
            result1[0] = BitConverter.ToUInt16(result, 0);
            result1[1] = BitConverter.ToUInt16(result, 2);
            return result1;
        }

        /// <summary>
        ///int32 转换为无符号16位整数
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">长度</param>
        /// <param name="type">字节类型</param>
        /// <returns></returns>
        public static ushort[] Int2Ushort(int data,Bytebyte type)
        {
            byte[] result = new byte[4];
            byte[] a = BitConverter.GetBytes(data);
            ushort[] result1 = new ushort[2];
            if (data != null)
            {
                switch (type)
                {
                    case Bytebyte.ABCD:
                        result[0] = a[0];
                        result[1] = a[1];
                        result[2] = a[2];
                        result[3] = a[3];
                        break;
                    case Bytebyte.CDAB:

                        result[0] = a[2]; ;
                        result[1] = a[3]; ;
                        result[2] = a[0];
                        result[3] = a[1];

                        break;
                    case Bytebyte.BADC:

                        result[0] = a[1];
                        result[1] = a[0];
                        result[2] = a[3];
                        result[3] = a[2];

                        break;
                    case Bytebyte.DCBA:
                        result[0] = a[3];
                        result[1] = a[2];
                        result[2] = a[1];
                        result[3] = a[0];

                        break;
                    default:
                        break;
                }
            }
            result1[0] = BitConverter.ToUInt16(result, 0);
            result1[1] = BitConverter.ToUInt16(result, 2);
            return result1;
        }
        /// <summary>
        ///int32[] 转换为无符号16位整数
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">长度</param>
        /// <param name="type">字节类型</param>
        /// <returns></returns>
        public static ushort[] Int2Ushort(int[] data, int startindex, Bytebyte type)
        {
            byte[] result = new byte[4];
            byte[] a = BitConverter.GetBytes(data[startindex]);
            ushort[] result1 = new ushort[2];
            if (data != null)
            {
                switch (type)
                {
                    case Bytebyte.ABCD:
                        result[0] = a[0];
                        result[1] = a[1];
                        result[2] = a[2];
                        result[3] = a[3];
                        break;
                    case Bytebyte.CDAB:

                        result[0] = a[2]; ;
                        result[1] = a[3]; ;
                        result[2] = a[0];
                        result[3] = a[1];

                        break;
                    case Bytebyte.BADC:

                        result[0] = a[1];
                        result[1] = a[0];
                        result[2] = a[3];
                        result[3] = a[2];

                        break;
                    case Bytebyte.DCBA:
                        result[0] = a[3];
                        result[1] = a[2];
                        result[2] = a[1];
                        result[3] = a[0];

                        break;
                    default:
                        break;
                }
            }
            result1[0] = BitConverter.ToUInt16(result, 0);
            result1[1] = BitConverter.ToUInt16(result, 2);
            return result1;
        }

        /// <summary>
        ///单精度浮点数 转换为无符号16位整数
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">长度</param>
        /// <param name="type">字节类型</param>
        /// <returns></returns>
        public static ushort[] Float2Ushort(float data, Bytebyte type)
        {
            byte[] result = new byte[4];
            byte[] a = BitConverter.GetBytes(data);
            ushort[] result1 = new ushort[2];
            if (data != null)
            {
                switch (type)
                {
                    case Bytebyte.ABCD:
                        result[0] = a[0];
                        result[1] = a[1];
                        result[2] = a[2];
                        result[3] = a[3];
                        break;
                    case Bytebyte.CDAB:

                        result[0] = a[2]; ;
                        result[1] = a[3]; ;
                        result[2] = a[0];
                        result[3] = a[1];

                        break;
                    case Bytebyte.BADC:

                        result[0] = a[1];
                        result[1] = a[0];
                        result[2] = a[3];
                        result[3] = a[2];

                        break;
                    case Bytebyte.DCBA:
                        result[0] = a[3];
                        result[1] = a[2];
                        result[2] = a[1];
                        result[3] = a[0];

                        break;
                    default:
                        break;
                }
            }
            result1[0] = BitConverter.ToUInt16(result, 0);
            result1[1] = BitConverter.ToUInt16(result, 2);
            return result1;
        }
        /// <summary>
        ///float[] 转换为无符号16位整数
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="startpos">开始位置</param>
        /// <param name="length">长度</param>
        /// <param name="type">字节类型</param>
        /// <returns></returns>
        public static ushort[] Float2Ushort(float[] data, int startindex, Bytebyte type)
        {
            byte[] result = new byte[4];
            byte[] a = BitConverter.GetBytes(data[startindex]);
            ushort[] result1 = new ushort[2];
            if (data != null)
            {
                switch (type)
                {
                    case Bytebyte.ABCD:
                        result[0] = a[0];
                        result[1] = a[1];
                        result[2] = a[2];
                        result[3] = a[3];
                        break;
                    case Bytebyte.CDAB:

                        result[0] = a[2]; ;
                        result[1] = a[3]; ;
                        result[2] = a[0];
                        result[3] = a[1];

                        break;
                    case Bytebyte.BADC:

                        result[0] = a[1];
                        result[1] = a[0];
                        result[2] = a[3];
                        result[3] = a[2];

                        break;
                    case Bytebyte.DCBA:
                        result[0] = a[3];
                        result[1] = a[2];
                        result[2] = a[1];
                        result[3] = a[0];

                        break;
                    default:
                        break;
                }
            }
            result1[0] = BitConverter.ToUInt16(result, 0);
            result1[1] = BitConverter.ToUInt16(result, 2);
            return result1;
        }
    }
}

