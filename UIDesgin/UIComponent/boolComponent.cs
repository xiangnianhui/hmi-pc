﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UIDesgin.UIComponent
{
    public partial class boolComponent : CheckBox
    {
        public boolComponent()
        {
            InitializeComponent();
        }

        public boolComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        [DefaultValue(0), Description("bool型控件"), Category("BinUI")]
        public int ID { get; set; }
        [DefaultValue(0), Description("bool型控件"), Category("BinUI")]
        public int Address { get; set; }
        [DefaultValue(""), Description("bool型控件"), Category("BinUI")]
        public string Describe { get; set; }

    }
}
