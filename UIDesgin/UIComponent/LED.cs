﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UIDesgin.UIComponent
{
    
    public partial class LED : UserControl
    {
        public LED()
        {
            InitializeComponent();
            //1.设置缓冲属性
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.Selectable, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);
        }
        //2.定义三个画图的字段
        #region 字段
        private Graphics g;
        private Pen p;
        private SolidBrush sb;
        #endregion
        //3.添加一个设置Graphics的方法
        #region 设置Graphics属性
        private void SetGraohics(Graphics g)
        {
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
        }
        #endregion
        //4.根据实际控件分析结果，创建属性
        #region  控件属性
        private Color ledColor=Color.LimeGreen;
        [Category("BinUi"),Description("led指示TRUE灯的颜色")]
        public Color LedColor
        {
            get { return ledColor; }
            set { ledColor = value; this.Invalidate(); }
        }

        private Color ledflaseColor = Color.Red;
        [Category("BinUi"), Description("led指示FLASE灯的颜色")]
        public Color LedflaseColor
        {
            get { return ledflaseColor; }
            set { ledflaseColor = value; this.Invalidate(); }
        }

        private bool bvalue=true;
        [Category("BinUi"), Description("led灯的值")]
        public bool bValue
        {
            get { return bvalue; }
            set { bvalue = value; this.Invalidate(); }
        }

        private bool isBoder=true;
        [Category("BinUi"), Description("是否画圆环")]
        public bool IsBoder
        {
            get { return isBoder; }
            set { isBoder = value;this.Invalidate();  }
        }

        private int boderWidth = 5;
        [Category("BinUi"), Description("圆环宽度")]
        public int BoderWidth
        {
            get { return boderWidth; }
            set { boderWidth = value; this.Invalidate(); }
        }

        private int gapWidth = 5;
        [Category("BinUi"), Description("间隙宽度")]
        public int GapWidth
        {
            get { return gapWidth; }
            set { gapWidth = value; this.Invalidate(); }
        }


        #endregion

        //5.创建重回事件
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Color color=bvalue?LedColor:ledflaseColor;
            g = e.Graphics;
            SetGraohics(g);
            sb = new SolidBrush(color);
            RectangleF rec = new RectangleF(1, 1, this.Width - 2, this.Height - 2);
            g.FillEllipse(sb, rec);
            if (isBoder)
            {
             
                p = new Pen(Color.White, boderWidth);
                float x = 1 + gapWidth + boderWidth * 0.5f;
                RectangleF rec1 = new RectangleF(x, x, (float)this.Width - 2*x, (float)this.Height - 2*x);
                g.DrawEllipse(p, rec1);
           
            }

        }



    }
}
