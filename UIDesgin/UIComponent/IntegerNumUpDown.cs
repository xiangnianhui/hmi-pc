﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing;
using UIDesgin.UIMessage;
using Models;

namespace UIDesgin.UIComponent
{
    [DefaultEvent("Click")]
    [DefaultProperty("Text")]
    public partial class IntegerNumUpDown : UserControl
    {
        #region 自定义事件

        public delegate void ParamSetDelegete(object sender, EventArgs e);
        [Category("BinUi"),Description("PLC参数设置事件")]
        public event ParamSetDelegete ParamSetEvent;

        #endregion


        public IntegerNumUpDown()
        {
            InitializeComponent();

        }
        #region 控件属性
        [DefaultValue(0), Description("PLC地址"), Category("BinUI")]
        public int Address { get; set; }

        [DefaultValue(9999999), Description("最大值"), Category("BinUI")]
        public int MaxValue { get; set; } = 9999999;

        [DefaultValue(0), Description("最小值"), Category("BinUI")]
        public int MinValue { get; set; }

        [DefaultValue(1), Description("步进值"), Category("BinUI")]
        public int Step { get; set; } = 1;

        [DefaultValue(typeof(DataType), "Xint"), Description("数据类型"), Category("BinUI")]
        public PlcDatatype Type { get; set; } = PlcDatatype.Ushort;

        //当前值
        private int _value = 0;
        [DefaultValue(0), Description("值"), Category("BinUI")]
        public int Value
        {
            get { return _value; }
            set
            {
                _value = value;
                this.btnValue.Text = value.ToString();
            }
        }
        #endregion

        #region  值检测
        private int ValueCheck(int value)
        {
            if (value > this.MaxValue)
            {
                return (value = this.MaxValue);
            }
            if (value < this.MinValue)
            {
                return (value = MinValue);
            }
            return value;
        }
        #endregion

        //数值递增
        private void BtnAdd_Click(object sender, EventArgs e)
        {
            int result = this.Value + this.Step;
            this.Value = ValueCheck(result);
            this.btnValue.Text = this.Value.ToString();

        }
        //数值递减
        private void BtnSub_Click(object sender, EventArgs e)
        {
            int result = this.Value - this.Step;
            this.Value = ValueCheck(result);
            this.btnValue.Text = this.Value.ToString();

        }
        //值输入
        private void btnValue_Click(object sender, EventArgs e)
        {
            ParamSetEvent?.Invoke(this,e);
                    int value = int.Parse(btnValue.Text);
                    UIMessage.MessageHelper.InputIntergerDialog(ref value, this.MinValue, this.MaxValue, true);
                    Value = value;        
        }

      
    }
}
