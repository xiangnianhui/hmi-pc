﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;

namespace UIDesgin.UIComponent
{
    public partial class VarMesSend : UserControl
    {

        #region 自定义事件    
        public delegate void ParamSetDelegete(object sender, EventArgs e);

        [Category("BinUi"), Description("PLC参数设置事件")]
        public event ParamSetDelegete ParamSetEvent;
        #endregion

        public VarMesSend()
        {
            InitializeComponent();           
        }
      
        #region 属性
        private string varName= "变量名称";
        [Category("BinUI"),Description("变量名称")]
        public string VarName
        {
            get { return this.labVarName.Text; }
            set { this.labVarName.Text = value;  }
        }

        private string varUnit="单位";
        [Category("BinUI"), Description("变量单位")]
        public string VarUnit
        {
            get { return this.labVarUnit.Text; }
            set { this.labVarUnit.Text = value;  }
        }

        [Category("BinUI"), Description("变量单位")]
        public string  Value
        {
            get
            {
                if (btnVarValue.Text!=null)
                {
                    return this.btnVarValue.Text;
                }
                return "0";
            }
            set { this.btnVarValue.Text = value.ToString(); }
        }

        [Category("BinUI"), Description("变量地址")]
        public string Address { get; set; }
        [Category("BinUI"), Description("变量类型")]
        public PlcDatatype Type { get; set; }
        #endregion


        private void btnVarValue_Click(object sender, EventArgs e)
        {
            ParamSetEvent?.Invoke(this, e);
        }
    }
}
