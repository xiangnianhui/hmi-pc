﻿namespace UIDesgin.UIComponent
{
    partial class VarMesSend
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labVarUnit = new System.Windows.Forms.Label();
            this.labVarName = new System.Windows.Forms.Label();
            this.btnVarValue = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(312, 35);
            this.panel1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel1.Controls.Add(this.labVarUnit, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.labVarName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnVarValue, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(310, 33);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // labVarUnit
            // 
            this.labVarUnit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labVarUnit.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.labVarUnit.Location = new System.Drawing.Point(257, 5);
            this.labVarUnit.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.labVarUnit.Name = "labVarUnit";
            this.labVarUnit.Size = new System.Drawing.Size(50, 24);
            this.labVarUnit.TabIndex = 2;
            this.labVarUnit.Text = "单位";
            // 
            // labVarName
            // 
            this.labVarName.BackColor = System.Drawing.Color.Transparent;
            this.labVarName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labVarName.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.labVarName.Location = new System.Drawing.Point(15, 5);
            this.labVarName.Margin = new System.Windows.Forms.Padding(15, 5, 3, 0);
            this.labVarName.Name = "labVarName";
            this.labVarName.Size = new System.Drawing.Size(104, 24);
            this.labVarName.TabIndex = 0;
            this.labVarName.Text = "变量名称";
            // 
            // btnVarValue
            // 
            this.btnVarValue.AutoSize = true;
            this.btnVarValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnVarValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVarValue.Location = new System.Drawing.Point(125, 3);
            this.btnVarValue.Name = "btnVarValue";
            this.btnVarValue.Size = new System.Drawing.Size(114, 27);
            this.btnVarValue.TabIndex = 3;
            this.btnVarValue.Text = "0";
            this.btnVarValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVarValue.UseVisualStyleBackColor = true;
            this.btnVarValue.Click += new System.EventHandler(this.btnVarValue_Click);
            // 
            // VarMesSend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "VarMesSend";
            this.Size = new System.Drawing.Size(312, 35);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labVarUnit;
        private System.Windows.Forms.Label labVarName;
        private System.Windows.Forms.Button btnVarValue;
    }
}
