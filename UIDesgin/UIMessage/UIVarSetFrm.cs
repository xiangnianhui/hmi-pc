﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;
using DAL;

namespace UIDesgin.UIMessage
{
    public partial class UIVarSetFrm : Form
    {
        public UIVarSetFrm(ModbusClient client, PlcVarValue plcVar,bool Iswriteplc=true)
        {
            InitializeComponent();
            this.txtSetValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlyNumberInput);
            this.IsWritePlc = Iswriteplc;
            this.client = client;
            this.plcvar = plcVar;
            if (plcVar!=null)
            {
                plcvar = plcVar;
                this.labVarName.Text = plcVar.VarName;
                this.labVarType.Text = plcVar.Vartype.ToString();
                this.labVarValue.Text = plcVar.VarValue.ToString();
                this.txtSetValue.Text = plcVar.VarValue.ToString();
                Address =plcVar.VarAddress;
            }
           
        }

        #region 之前属性
        ////整数最小值
        //public int MinValue 
        //{
        //    get { return int.Parse(labMinValue.Text); }
        //    set { labMinValue.Text = value.ToString(); }
        //}

        ////整数最大值
        //public int MaxValue
        //{
        //    get { return int.Parse(labMaxValue.Text); }
        //    set { labMaxValue.Text = value.ToString(); }
        //}
        ////描述
        //public string Desc { get =>this.labMaxValue.Text;
        //    set=>this.labDescName.Text=value; }

        ////数值
        //public int InputValue
        //{
        //    get { return int.Parse(txtSetValue.Text); }
        //    set { txtSetValue.Text = value.ToString(); }
        //}

        //public int ValueCheck(ref int value)
        //{
        //    int _value;
        //    if (int.TryParse(txtSetValue.Text, out _value))
        //    {
        //        if (_value > MaxValue)
        //        {
        //            return value=MaxValue;
        //        }
        //        if (_value < MinValue)
        //        {
        //            return value = MinValue;
        //        }
        //        return value = _value;
        //    }
        //    else
        //    {
        //        return value;
        //    }
        //}
        #endregion

        #region  字段
        //最小值
        private object minValue;
        //最大值
        private object maxValue;
        ModbusClient client;

        PlcVarValue plcvar;
        //是否写入plc
        private bool IsWritePlc;

        #endregion

        #region  //属性
        public bool IsOk { get; set; }
        //地址
        public string Address { get; set; }
        //数值
        public object Value { get { return txtSetValue.Text; } set { txtSetValue.Text = value.ToString(); } }

        #endregion

        //只能输入数字
        private void onlyNumberInput(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == '.' || e.KeyChar == '-'))
                e.Handled = true;
            if (e.KeyChar == '\b')
                e.Handled = false;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (client!=null&& IsWritePlc)
            {
                switch (plcvar.Vartype)
                {
                    case PlcDatatype.Bool:
                        break;
                    case PlcDatatype.Short:
                        break;
                    case PlcDatatype.Ushort:
                        client.WriteSingleRegister(1, Convert.ToUInt16(plcvar.VarAddress), Convert.ToUInt16(this.Value));
                        break;
                    case PlcDatatype.Int:
                        break;
                    case PlcDatatype.Uint:
                        break;
                    case PlcDatatype.Float:
                        break;
                    case PlcDatatype.Double:
                        break;
                    default:
                        break;
                }
            }
           
            IsOk = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            IsOk = false;
            this.Close();
        }
    }
}
