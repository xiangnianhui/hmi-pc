﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UIDesgin.UIMessage
{
    public partial class UIFrm : Form
    {
        public UIFrm()
        {
            InitializeComponent();
        }


        public bool IsOK { get; set; }

        private bool showcancel=true;
        public bool ShowCancel
        {
            get { return showcancel; }
            set {
                showcancel = value;
                    this.BtnCancel.Visible=value; }
        }

        public string  Title
        {
            get {return labTitle.Text; }
            set { labTitle.Text = value; }
        }



        public virtual void ShowMessage(string message, string title, bool showCancel)
        {

            Title = title;
            ShowCancel = showCancel;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            IsOK = true;
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            IsOK = false;
            this.Close();
        }
        
    }
}
