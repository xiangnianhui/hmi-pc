﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UIDesgin.UIMessage
{
    public partial class UIfloatInputFrm : UIFrm
    {
        public UIfloatInputFrm()
        {
            InitializeComponent();
            this.txtValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlyNumberInput);

        }
        //最小值
        public float MinValue 
        {
            get { return int.Parse(labMinValue.Text); }
            set { labMinValue.Text = value.ToString(); }
        }
        //最大值
        public float MaxValue
        {
            get { return int.Parse(labMaxValue.Text); }
            set { labMaxValue.Text = value.ToString(); }
        }
        //描述
        public string Desc { get =>this.labMaxValue.Text;
            set=>this.labDesc.Text=value; }

        //数值
        public float InputValue
        {
            get { return int.Parse(txtValue.Text); }
            set { txtValue.Text = value.ToString(); }
        }

        public float ValueCheck(ref float value)
        {
            if (float.TryParse(txtValue.Text, out value))
            {
                if (value > MaxValue)
                {
                    return value=MaxValue;
                }
                if (value < MinValue)
                {
                    return value = MinValue;
                }
            }           
                return value;                  
        }

        //只允许输入数字
        private void onlyNumberInput(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == '.'))
                e.Handled = true;
            if (e.KeyChar == '\b')
                e.Handled = false;
        }

       
    }
}
