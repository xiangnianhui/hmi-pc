﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UIDesgin.UIMessage
{
    public    enum DataType
    {
        XBool,//bool
        xushort,//无符号16bit整数数
        Xshort,//16bit整数
        Xint,//32位整数
        XUint,//无符号32bit整数
        Xfloat,//单精度浮点数
        Xdouble,//双精度浮点数

    }

    public static class MessageHelper
    {



        public static bool ShowMessageDialog( string desc, string title, bool isShowCancel)
        {
            UIMessageFrm frm = new UIMessageFrm();
            frm.TopMost = false;
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowMessage(desc, title, isShowCancel);
            frm.ShowDialog();
            bool isOk = frm.IsOK;
            frm.Dispose();
            return isOk;
        }

        //字符输入
        public static bool InputStringDialog(ref string str , string desc, string title)
        {
            UIStringInputFrm frm = new UIStringInputFrm();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowMessage(desc, title,true);
            frm.InputValue = str;
            frm.ShowDialog();
            if (frm.IsOK)
            {
                if (frm.InputValue!=null&&frm.InputValue.Trim().Length>0)
                {
                    str= frm.InputValue ;
                    return true;
                }
               
            }

            return false;
        }

        //整数输入
        public static bool InputIntergerDialog(ref int value, int minvalue = 0, int maxvalue = 2147483647, bool checkEmpty = true, string desc = "请输入整数：")
        {
            UIIntInputFrm frm = new UIIntInputFrm();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.Desc = desc;
            frm.MinValue = minvalue;
            frm.MaxValue = maxvalue;
            frm.InputValue = value;
            //  frm.CheckInputEmpty = checkEmpty;
            frm.ShowDialog();
            
          
            if (frm.IsOK)
            {
                frm.ValueCheck(ref value);
                frm.InputValue = value;
                return true;
            }

            return false;
        }

        //单精度浮点数输入
        public static bool InputFloatDialog(ref float value, float minvalue = 0, float maxvalue = float.MaxValue, bool checkEmpty = true, string desc = "请输入单精度浮点数：")
        {
            UIfloatInputFrm frm = new UIfloatInputFrm();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.Desc = desc;
            frm.MinValue = minvalue;
            frm.MaxValue = maxvalue;
            frm.InputValue = value;
            //  frm.CheckInputEmpty = checkEmpty;
            frm.ShowDialog();
            frm.ValueCheck(ref value);
            if (frm.IsOK)
            {
                frm.InputValue = value;
                return true;
            }

            return false;
        }
    }
   
}

