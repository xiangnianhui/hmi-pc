﻿namespace UIDesgin.UIMessage
{
    partial class UIVarSetFrm
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.labDescName = new System.Windows.Forms.Label();
            this.txtSetValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labMinValue = new System.Windows.Forms.Label();
            this.labMaxValue = new System.Windows.Forms.Label();
            this.labVarName = new System.Windows.Forms.Label();
            this.labDecsType = new System.Windows.Forms.Label();
            this.labVarType = new System.Windows.Forms.Label();
            this.labVarValue = new System.Windows.Forms.Label();
            this.labDescCur = new System.Windows.Forms.Label();
            this.labDescSet = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labTitle = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labDescName
            // 
            this.labDescName.AutoSize = true;
            this.labDescName.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labDescName.Location = new System.Drawing.Point(12, 38);
            this.labDescName.Name = "labDescName";
            this.labDescName.Size = new System.Drawing.Size(74, 25);
            this.labDescName.TabIndex = 5;
            this.labDescName.Text = "变量名:";
            // 
            // txtSetValue
            // 
            this.txtSetValue.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtSetValue.Location = new System.Drawing.Point(288, 114);
            this.txtSetValue.Name = "txtSetValue";
            this.txtSetValue.Size = new System.Drawing.Size(162, 34);
            this.txtSetValue.TabIndex = 0;
            this.txtSetValue.Text = "0";
            this.txtSetValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(257, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "最小值:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(258, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "最大值:";
            // 
            // labMinValue
            // 
            this.labMinValue.BackColor = System.Drawing.Color.White;
            this.labMinValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labMinValue.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labMinValue.Location = new System.Drawing.Point(321, 43);
            this.labMinValue.Name = "labMinValue";
            this.labMinValue.Size = new System.Drawing.Size(129, 20);
            this.labMinValue.TabIndex = 9;
            this.labMinValue.Text = "0";
            this.labMinValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labMaxValue
            // 
            this.labMaxValue.BackColor = System.Drawing.Color.White;
            this.labMaxValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labMaxValue.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labMaxValue.Location = new System.Drawing.Point(321, 76);
            this.labMaxValue.Name = "labMaxValue";
            this.labMaxValue.Size = new System.Drawing.Size(129, 20);
            this.labMaxValue.TabIndex = 10;
            this.labMaxValue.Text = "0";
            this.labMaxValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labVarName
            // 
            this.labVarName.BackColor = System.Drawing.Color.Azure;
            this.labVarName.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labVarName.Location = new System.Drawing.Point(87, 40);
            this.labVarName.Name = "labVarName";
            this.labVarName.Size = new System.Drawing.Size(126, 25);
            this.labVarName.TabIndex = 11;
            // 
            // labDecsType
            // 
            this.labDecsType.AutoSize = true;
            this.labDecsType.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labDecsType.Location = new System.Drawing.Point(12, 75);
            this.labDecsType.Name = "labDecsType";
            this.labDecsType.Size = new System.Drawing.Size(55, 25);
            this.labDecsType.TabIndex = 12;
            this.labDecsType.Text = "类型:";
            // 
            // labVarType
            // 
            this.labVarType.BackColor = System.Drawing.Color.Azure;
            this.labVarType.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labVarType.Location = new System.Drawing.Point(87, 75);
            this.labVarType.Name = "labVarType";
            this.labVarType.Size = new System.Drawing.Size(126, 25);
            this.labVarType.TabIndex = 13;
            // 
            // labVarValue
            // 
            this.labVarValue.BackColor = System.Drawing.Color.Azure;
            this.labVarValue.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labVarValue.Location = new System.Drawing.Point(58, 114);
            this.labVarValue.Name = "labVarValue";
            this.labVarValue.Size = new System.Drawing.Size(160, 34);
            this.labVarValue.TabIndex = 14;
            this.labVarValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labDescCur
            // 
            this.labDescCur.AutoSize = true;
            this.labDescCur.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labDescCur.Location = new System.Drawing.Point(5, 119);
            this.labDescCur.Name = "labDescCur";
            this.labDescCur.Size = new System.Drawing.Size(50, 25);
            this.labDescCur.TabIndex = 15;
            this.labDescCur.Text = "当前";
            // 
            // labDescSet
            // 
            this.labDescSet.AutoSize = true;
            this.labDescSet.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labDescSet.Location = new System.Drawing.Point(232, 119);
            this.labDescSet.Name = "labDescSet";
            this.labDescSet.Size = new System.Drawing.Size(50, 25);
            this.labDescSet.TabIndex = 3;
            this.labDescSet.Text = "设定";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.labTitle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(467, 33);
            this.panel1.TabIndex = 17;
            // 
            // labTitle
            // 
            this.labTitle.AutoSize = true;
            this.labTitle.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labTitle.Location = new System.Drawing.Point(9, 3);
            this.labTitle.Name = "labTitle";
            this.labTitle.Size = new System.Drawing.Size(88, 25);
            this.labTitle.TabIndex = 0;
            this.labTitle.Text = "参数设置";
            // 
            // btnOk
            // 
            this.btnOk.AutoSize = true;
            this.btnOk.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOk.ForeColor = System.Drawing.SystemColors.InfoText;
            this.btnOk.Location = new System.Drawing.Point(274, 166);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(78, 37);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "确定";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AutoSize = true;
            this.btnCancel.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCancel.ForeColor = System.Drawing.SystemColors.InfoText;
            this.btnCancel.Location = new System.Drawing.Point(377, 166);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(78, 37);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // UIVarSetFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(467, 213);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labDescSet);
            this.Controls.Add(this.labDescCur);
            this.Controls.Add(this.labVarValue);
            this.Controls.Add(this.labVarType);
            this.Controls.Add(this.labDecsType);
            this.Controls.Add(this.labVarName);
            this.Controls.Add(this.labMaxValue);
            this.Controls.Add(this.labMinValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSetValue);
            this.Controls.Add(this.labDescName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UIVarSetFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labDescName;
        private System.Windows.Forms.TextBox txtSetValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labMinValue;
        private System.Windows.Forms.Label labMaxValue;
        private System.Windows.Forms.Label labVarName;
        private System.Windows.Forms.Label labDecsType;
        private System.Windows.Forms.Label labVarType;
        private System.Windows.Forms.Label labVarValue;
        private System.Windows.Forms.Label labDescCur;
        private System.Windows.Forms.Label labDescSet;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labTitle;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}
