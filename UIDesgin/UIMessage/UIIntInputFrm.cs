﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UIDesgin.UIMessage
{
    public partial class UIIntInputFrm : UIFrm
    {
        public UIIntInputFrm()
        {
            InitializeComponent();
            this.txtValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onlyNumberInput);
        }
        //整数最小值
        public int MinValue 
        {
            get { return int.Parse(labMinValue.Text); }
            set { labMinValue.Text = value.ToString(); }
        }

        //整数最大值
        public int MaxValue
        {
            get { return int.Parse(labMaxValue.Text); }
            set { labMaxValue.Text = value.ToString(); }
        }
        //描述
        public string Desc { get =>this.labMaxValue.Text;
            set=>this.labVarName.Text=value; }

        //数值
        public int InputValue
        {
            get { return int.Parse(txtValue.Text); }
            set { txtValue.Text = value.ToString(); }
        }

        public int ValueCheck(ref int value)
        {
            int _value;
            if (int.TryParse(txtValue.Text, out _value))
            {
                if (_value > MaxValue)
                {
                    return value=MaxValue;
                }
                if (_value < MinValue)
                {
                    return value = MinValue;
                }
                return value = _value;
            }
            else
            {
                return value;
            }
                              
        }
        //只能输入数字
        private void onlyNumberInput(object sender, KeyPressEventArgs e)
        {
            if (!(e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == '.'))
                e.Handled = true;
            if (e.KeyChar == '\b')
                e.Handled = false;
        }

    }
}
