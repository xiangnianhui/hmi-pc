﻿namespace UIDesgin.UIMessage
{
    partial class UIfloatInputFrm
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.labDesc = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labMinValue = new System.Windows.Forms.Label();
            this.labMaxValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labDesc
            // 
            this.labDesc.AutoSize = true;
            this.labDesc.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labDesc.Location = new System.Drawing.Point(26, 55);
            this.labDesc.Name = "labDesc";
            this.labDesc.Size = new System.Drawing.Size(145, 25);
            this.labDesc.TabIndex = 5;
            this.labDesc.Text = "输入一个浮点数";
            // 
            // txtValue
            // 
            this.txtValue.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtValue.Location = new System.Drawing.Point(31, 107);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(402, 34);
            this.txtValue.TabIndex = 6;
            this.txtValue.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(240, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "最小值:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(241, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "最大值:";
            // 
            // labMinValue
            // 
            this.labMinValue.BackColor = System.Drawing.Color.White;
            this.labMinValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labMinValue.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labMinValue.Location = new System.Drawing.Point(304, 44);
            this.labMinValue.Name = "labMinValue";
            this.labMinValue.Size = new System.Drawing.Size(129, 20);
            this.labMinValue.TabIndex = 9;
            this.labMinValue.Text = "0";
            this.labMinValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labMaxValue
            // 
            this.labMaxValue.BackColor = System.Drawing.Color.White;
            this.labMaxValue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labMaxValue.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labMaxValue.Location = new System.Drawing.Point(304, 72);
            this.labMaxValue.Name = "labMaxValue";
            this.labMaxValue.Size = new System.Drawing.Size(129, 20);
            this.labMaxValue.TabIndex = 10;
            this.labMaxValue.Text = "0";
            this.labMaxValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // UIfloatInputFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(467, 213);
            this.Controls.Add(this.labMaxValue);
            this.Controls.Add(this.labMinValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.labDesc);
            this.Name = "UIfloatInputFrm";
            this.Controls.SetChildIndex(this.labDesc, 0);
            this.Controls.SetChildIndex(this.txtValue, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.labMinValue, 0);
            this.Controls.SetChildIndex(this.labMaxValue, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labDesc;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labMinValue;
        private System.Windows.Forms.Label labMaxValue;
    }
}
