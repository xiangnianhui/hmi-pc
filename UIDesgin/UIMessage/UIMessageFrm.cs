﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UIDesgin.UIMessage
{
    public partial class UIMessageFrm : UIFrm
    {
        public UIMessageFrm()
        {
            InitializeComponent();
        }
        public string Message { get=> this.label1.Text;
            set=> this.label1.Text=value; }

        public override void ShowMessage(string message, string title, bool showCancel)
        {
          //  base.ShowMessage(message, title, showCancel);
            Message = message;
            base.Title = title;
        }

    }


    
}
