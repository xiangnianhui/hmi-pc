﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Models;
using DAL;
using System.Configuration;

namespace BinHmiSystem
{
    public partial class FrmLogin : Form
    {

        public delegate void UserSend(User user);
        public UserSend usersend;
        Userservice userservice = new Userservice();

        #region 窗口移动
        private Point mouseOff;//鼠标移动位置变量
        private bool leftFlag;//鼠标是否为左键
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//获得当前鼠标的坐标
                leftFlag = true;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;//获得移动后鼠标的坐标
                mouseSet.Offset(mouseOff.X, mouseOff.Y);//设置移动后的位置
                Location = mouseSet;
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;
            }
        }
        #endregion

        #region config 读写操作
        //用户管理存储路径
        string username, userpwd, Rememberpwd;
        //实例化一个配置文件类

        private void readCofig()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            try
            {
                username = config.AppSettings.Settings["username"].Value;
                userpwd = config.AppSettings.Settings["userpwd"].Value;
                Rememberpwd = config.AppSettings.Settings["Rememberpwd"].Value;
            }
            catch (Exception)
            {
                //MessageBox.Show("配置文件错误,无法导入订单数据");
            }


        }
        private void writeCofig()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["username"].Value = username;
            config.AppSettings.Settings["userpwd"].Value = userpwd;
            config.AppSettings.Settings["Rememberpwd"].Value = Rememberpwd;
            config.Save(ConfigurationSaveMode.Modified);
        }

        #endregion

        public FrmLogin()
        {
            InitializeComponent();
            FrmInit();
        }
        //初始化
        private void FrmInit()
        {
            //读取用户信息
            userservice.Users = SerializeHelper<User>.BinarySerialzeRead("用户信息");
            //检测是否有管理员用户
            if (!userservice.CheckAdmin())
            {
                User user = userservice.CreateAdmin();
                userservice.Users.Add(user);
                SerializeHelper<User>.BinarySerialzeWrite("用户信息", userservice.Users);
            }
            //读取配置文件
            readCofig();
            this.txtUserName.Text = username;
            this.txtUserPwd.Text = userpwd;
            //三元运算符  
            CBpwd.Checked = Rememberpwd == "true" ? true : false;

        }
        //登陆检查
        private void LoginCheck()
        {
            string name = txtUserName.Text.Trim();
            string pwd = txtUserPwd.Text;
            if (string.IsNullOrEmpty(name.Trim()))
            {
                MessageBox.Show("用户名不能为空", "提示");
                return;
            }
            if (string.IsNullOrEmpty(pwd))
            {
                MessageBox.Show("密码不能为空", "提示");
                return;
            }
            //检查是否存在这个用户
            User user = userservice.CheckExistUser(name, pwd);
            if (user != null)
            {
                // MessageBox.Show("登录成功", "提示");
                Userservice.Currentuser = user;
                //检查记住站号密码是否选中
                if (CBpwd.Checked)
                {
                    username = name;
                    userpwd = pwd;
                    Rememberpwd = "true";
                }
                else
                {
                    userpwd = "";
                    Rememberpwd = "false";
                }
                //写入配置文件
                writeCofig();
                //切换至运行界面
                Frmchange();
            }
            else
            {
                MessageBox.Show("用户或者密码错误", "提示");
            }
        }
        //界面切换
        private void Frmchange() 
        {
            CloseTimer.Start();
        }
        //窗口导入
        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }
        //窗口口关闭
        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //淡出计时器
        private void CloseTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity >= 0.5)
            {
                this.Opacity -= 0.015;
            }
            else
            {
                CloseTimer.Stop();
                this.Close();
            }
        }
    
        //窗口最小化
        private void BtnWinMin_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        //登陆按钮
        private void btnLogin_Click_1(object sender, EventArgs e)
        {
            LoginCheck();
        }
    }
    
}
