﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;
using UIDesgin.UIMessage;
using System.IO;
using System.Configuration;

namespace BinHmiSystem
{
    public partial class FrmAlarmEdit : Form
    {
        //上传路径
        string Uppath;
        //下载路径
        string DownPath;
        #region 配置文件读写
        private void ConfigRead()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            DownPath = config.AppSettings.Settings["AlarmList"].Value.ToString();
        }
        #endregion
        public FrmAlarmEdit()
        {
            InitializeComponent();
            //自动生成新的列 设置为false
            DGWAlarmEdit.AutoGenerateColumns = false;
            //新建一个下拉列表列
            DataGridViewComboBoxColumn type = (DataGridViewComboBoxColumn)DGWAlarmEdit.Columns["Vartype"];
            //建立数据来源  plcdatatype的枚举变量
           type.DataSource = new PlcDatatype[] { PlcDatatype.Bool, PlcDatatype.Short, PlcDatatype.Ushort, PlcDatatype.Uint, PlcDatatype.Int, PlcDatatype.Float, PlcDatatype.Double, };
            //值类型为枚举类型
            type.ValueType = typeof(PlcDatatype);
            ConfigRead();
        }
        //打开文件
        private void btnOpen_Click(object sender, EventArgs e)
        {
            //打开文件窗口
            OpenFileDialog openFile = new OpenFileDialog();
            if (openFile.ShowDialog()==DialogResult.OK)
            {
                txtPath.Text = openFile.FileName;//文本内容等于打开的文件名称
                Uppath = txtPath.Text;//上传路径=文本内容
            } 
        }
        //上传到datagridview
        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtPath.Text.Trim()=="")
            {
                MessageBox.Show("本地路径不能为空");
            }
        DGWAlarmEdit.DataSource= SerializeHelper<PlcAlarmCoil>.XmlSerializeRead(txtPath.Text);
        }
        //点击数据表格
        private void DGWAlarmEdit_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex<0)
            {
                return;
            }
            DataGridViewCell cell = DGWAlarmEdit.Rows[e.RowIndex].Cells[e.ColumnIndex];
            if (cell.FormattedValue.ToString()=="删除")
            {
                MessageBox.Show("删除中");
            }
        }
        //另存为
        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            string path = null;
            //DataInit();
            if (MessageHelper.InputStringDialog(ref path, "输入配方名称", "配方保存"))
            {
                SerializeHelper<PlcAlarmCoil>.XmlSerializeWriter("AlarmRecipe\\" + path + ".xml", new List<PlcAlarmCoil>());
                MessageBox.Show("sucess");
                return;
            }
            MessageBox.Show("fail");

        }
        //报警配方保存
        private void btnSave_Click(object sender, EventArgs e)
        {
            List<PlcAlarmCoil> coils = (List<PlcAlarmCoil>)DGWAlarmEdit.DataSource;
            if (DialogResult.OK!=MessageBox.Show("是否保存","提示",MessageBoxButtons.OKCancel,MessageBoxIcon.Question))
            {
                return;
            }
            try
            {
                if (File.Exists(Uppath)&&coils.Count>0)
                {
                    SerializeHelper<PlcAlarmCoil>.XmlSerializeWriter(Uppath, coils);
                    MessageBox.Show("保存成功");
                }
                else
                {
                    MessageBox.Show("保存失败");
                }
               
            }
            catch (Exception E)
            {
                MessageBox.Show("保存失败   " + E.ToString()); 
            }
           
        }
        //下载报警至报警表单
        private void btnDownLoad_Click(object sender, EventArgs e)
        {
            List<PlcAlarmCoil> coils = (List<PlcAlarmCoil>)DGWAlarmEdit.DataSource;
            if (DialogResult.OK != MessageBox.Show("是否下载到系统", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question))
            {
                return;
            }
            try
            {
                if (!File.Exists(DownPath))
                {
                    File.Create(DownPath);
                }
                if (coils!=null)
                {
                    if (coils.Count > 0)
                    {
                        DGWAlarmEdit.DataSource = null;
                        SerializeHelper<PlcAlarmCoil>.XmlSerializeWriter(DownPath, coils);
                        DGWAlarmEdit.DataSource = coils;
                        MessageBox.Show("下载成功");

                    }
                    else
                    {
                        MessageBox.Show("数据不能为空");
                    }
                }
                else
                {
                    MessageBox.Show("数据不能为空");
                }
               

            }
            catch (Exception E)
            {
                MessageBox.Show("下载出错   " + E.ToString());
            }
            
        }
    }
}
