﻿namespace BinHmiSystem
{
    partial class FrmRecipeLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DGVRecipe = new System.Windows.Forms.DataGridView();
            this.recipeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recipeView = new System.Windows.Forms.DataGridViewLinkColumn();
            this.recipeDownload = new System.Windows.Forms.DataGridViewLinkColumn();
            this.recipeDel = new System.Windows.Forms.DataGridViewLinkColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnDGVplcVarClose = new Sunny.UI.UISymbolButton();
            this.label2 = new System.Windows.Forms.Label();
            this.DGVPlcVar = new System.Windows.Forms.DataGridView();
            this.vName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uiProcessBar1 = new Sunny.UI.UIProcessBar();
            ((System.ComponentModel.ISupportInitialize)(this.DGVRecipe)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPlcVar)).BeginInit();
            this.SuspendLayout();
            // 
            // DGVRecipe
            // 
            this.DGVRecipe.AllowUserToDeleteRows = false;
            this.DGVRecipe.AllowUserToResizeColumns = false;
            this.DGVRecipe.AllowUserToResizeRows = false;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.DGVRecipe.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.DGVRecipe.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVRecipe.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DGVRecipe.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("幼圆", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVRecipe.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.DGVRecipe.ColumnHeadersHeight = 40;
            this.DGVRecipe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGVRecipe.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.recipeName,
            this.recipeView,
            this.recipeDownload,
            this.recipeDel});
            this.DGVRecipe.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.DGVRecipe.Location = new System.Drawing.Point(9, 50);
            this.DGVRecipe.Margin = new System.Windows.Forms.Padding(0);
            this.DGVRecipe.MultiSelect = false;
            this.DGVRecipe.Name = "DGVRecipe";
            this.DGVRecipe.RowHeadersWidth = 51;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.DGVRecipe.RowsDefaultCellStyle = dataGridViewCellStyle31;
            this.DGVRecipe.RowTemplate.Height = 27;
            this.DGVRecipe.Size = new System.Drawing.Size(1194, 621);
            this.DGVRecipe.TabIndex = 0;
            this.DGVRecipe.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVRecipe_CellContentClick);
            // 
            // recipeName
            // 
            this.recipeName.DataPropertyName = "Name";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.recipeName.DefaultCellStyle = dataGridViewCellStyle27;
            this.recipeName.FillWeight = 265.3839F;
            this.recipeName.HeaderText = "配方名称";
            this.recipeName.MinimumWidth = 6;
            this.recipeName.Name = "recipeName";
            this.recipeName.ReadOnly = true;
            this.recipeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // recipeView
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle28.NullValue = "预览";
            this.recipeView.DefaultCellStyle = dataGridViewCellStyle28;
            this.recipeView.FillWeight = 46.90938F;
            this.recipeView.HeaderText = "配方预览";
            this.recipeView.MinimumWidth = 6;
            this.recipeView.Name = "recipeView";
            this.recipeView.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.recipeView.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.recipeView.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // recipeDownload
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle29.NullValue = "下载";
            this.recipeDownload.DefaultCellStyle = dataGridViewCellStyle29;
            this.recipeDownload.FillWeight = 44.92597F;
            this.recipeDownload.HeaderText = "配方下载";
            this.recipeDownload.MinimumWidth = 6;
            this.recipeDownload.Name = "recipeDownload";
            this.recipeDownload.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // recipeDel
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle30.NullValue = "删除";
            this.recipeDel.DefaultCellStyle = dataGridViewCellStyle30;
            this.recipeDel.FillWeight = 42.78075F;
            this.recipeDel.HeaderText = "配方删除";
            this.recipeDel.MinimumWidth = 6;
            this.recipeDel.Name = "recipeDel";
            this.recipeDel.VisitedLinkColor = System.Drawing.Color.Blue;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(278, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(626, 46);
            this.label1.TabIndex = 1;
            this.label1.Text = "配方列表";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnDGVplcVarClose);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.DGVPlcVar);
            this.panel1.Location = new System.Drawing.Point(174, 66);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(811, 532);
            this.panel1.TabIndex = 2;
            this.panel1.Visible = false;
            // 
            // BtnDGVplcVarClose
            // 
            this.BtnDGVplcVarClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnDGVplcVarClose.FillColor = System.Drawing.Color.Red;
            this.BtnDGVplcVarClose.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.BtnDGVplcVarClose.Location = new System.Drawing.Point(764, 3);
            this.BtnDGVplcVarClose.Name = "BtnDGVplcVarClose";
            this.BtnDGVplcVarClose.RectColor = System.Drawing.Color.Black;
            this.BtnDGVplcVarClose.Size = new System.Drawing.Size(32, 32);
            this.BtnDGVplcVarClose.Style = Sunny.UI.UIStyle.Custom;
            this.BtnDGVplcVarClose.Symbol = 61527;
            this.BtnDGVplcVarClose.SymbolSize = 30;
            this.BtnDGVplcVarClose.TabIndex = 2;
            this.BtnDGVplcVarClose.Click += new System.EventHandler(this.BtnDGVplcVarClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(340, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "变量预览";
            // 
            // DGVPlcVar
            // 
            this.DGVPlcVar.AllowUserToAddRows = false;
            this.DGVPlcVar.AllowUserToDeleteRows = false;
            this.DGVPlcVar.AllowUserToResizeColumns = false;
            this.DGVPlcVar.AllowUserToResizeRows = false;
            this.DGVPlcVar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVPlcVar.BackgroundColor = System.Drawing.SystemColors.Info;
            this.DGVPlcVar.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.DGVPlcVar.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVPlcVar.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.DGVPlcVar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVPlcVar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.vName,
            this.address,
            this.Type,
            this.Value});
            this.DGVPlcVar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DGVPlcVar.GridColor = System.Drawing.Color.Bisque;
            this.DGVPlcVar.Location = new System.Drawing.Point(0, 41);
            this.DGVPlcVar.Name = "DGVPlcVar";
            this.DGVPlcVar.ReadOnly = true;
            this.DGVPlcVar.RowHeadersWidth = 51;
            this.DGVPlcVar.RowTemplate.Height = 27;
            this.DGVPlcVar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVPlcVar.Size = new System.Drawing.Size(811, 491);
            this.DGVPlcVar.TabIndex = 0;
            // 
            // vName
            // 
            this.vName.DataPropertyName = "VarName";
            this.vName.FillWeight = 171.123F;
            this.vName.HeaderText = "变量名称";
            this.vName.MinimumWidth = 6;
            this.vName.Name = "vName";
            this.vName.ReadOnly = true;
            // 
            // address
            // 
            this.address.DataPropertyName = "VarAddress";
            this.address.FillWeight = 76.29234F;
            this.address.HeaderText = "变量地址";
            this.address.MinimumWidth = 6;
            this.address.Name = "address";
            this.address.ReadOnly = true;
            // 
            // Type
            // 
            this.Type.DataPropertyName = "Vartype";
            this.Type.FillWeight = 76.29234F;
            this.Type.HeaderText = "变量类型";
            this.Type.MinimumWidth = 6;
            this.Type.Name = "Type";
            this.Type.ReadOnly = true;
            // 
            // Value
            // 
            this.Value.DataPropertyName = "VarValue";
            this.Value.FillWeight = 76.29234F;
            this.Value.HeaderText = "变量值";
            this.Value.MinimumWidth = 6;
            this.Value.Name = "Value";
            this.Value.ReadOnly = true;
            // 
            // uiProcessBar1
            // 
            this.uiProcessBar1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiProcessBar1.Location = new System.Drawing.Point(87, 627);
            this.uiProcessBar1.MinimumSize = new System.Drawing.Size(70, 23);
            this.uiProcessBar1.Name = "uiProcessBar1";
            this.uiProcessBar1.Size = new System.Drawing.Size(972, 34);
            this.uiProcessBar1.TabIndex = 3;
            this.uiProcessBar1.Text = "0.0%";
            // 
            // FrmRecipeLoad
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1212, 673);
            this.Controls.Add(this.uiProcessBar1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DGVRecipe);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmRecipeLoad";
            this.Text = "FrmRecipeLoad";
            this.Load += new System.EventHandler(this.FrmRecipeLoad_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGVRecipe)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPlcVar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DGVRecipe;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView DGVPlcVar;
        private Sunny.UI.UISymbolButton BtnDGVplcVarClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn vName;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn recipeName;
        private System.Windows.Forms.DataGridViewLinkColumn recipeView;
        private System.Windows.Forms.DataGridViewLinkColumn recipeDownload;
        private System.Windows.Forms.DataGridViewLinkColumn recipeDel;
        private Sunny.UI.UIProcessBar uiProcessBar1;
    }
}