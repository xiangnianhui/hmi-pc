﻿namespace BinHmiSystem
{
    partial class FrmAlarmView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listAlarm = new System.Windows.Forms.ListBox();
            this.labTitle = new System.Windows.Forms.Label();
            this.BtnInfoUpdate = new Sunny.UI.UIButton();
            this.BtnInfoDown = new Sunny.UI.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uiTextBox1 = new Sunny.UI.UITextBox();
            this.uiTextBox2 = new Sunny.UI.UITextBox();
            this.uiTextBox3 = new Sunny.UI.UITextBox();
            this.uiTextBox4 = new Sunny.UI.UITextBox();
            this.uiTextBox5 = new Sunny.UI.UITextBox();
            this.uiTextBox6 = new Sunny.UI.UITextBox();
            this.uiTextBox7 = new Sunny.UI.UITextBox();
            this.uiTextBox8 = new Sunny.UI.UITextBox();
            this.uiTextBox9 = new Sunny.UI.UITextBox();
            this.uiTextBox10 = new Sunny.UI.UITextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // listAlarm
            // 
            this.listAlarm.Font = new System.Drawing.Font("微软雅黑", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listAlarm.FormattingEnabled = true;
            this.listAlarm.ItemHeight = 30;
            this.listAlarm.Location = new System.Drawing.Point(12, 90);
            this.listAlarm.Name = "listAlarm";
            this.listAlarm.Size = new System.Drawing.Size(992, 544);
            this.listAlarm.TabIndex = 0;
            // 
            // labTitle
            // 
            this.labTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTitle.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTitle.Location = new System.Drawing.Point(262, 9);
            this.labTitle.Name = "labTitle";
            this.labTitle.Size = new System.Drawing.Size(310, 39);
            this.labTitle.TabIndex = 1;
            this.labTitle.Text = "实时报警记录";
            this.labTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnInfoUpdate
            // 
            this.BtnInfoUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnInfoUpdate.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.BtnInfoUpdate.Location = new System.Drawing.Point(1032, 54);
            this.BtnInfoUpdate.Name = "BtnInfoUpdate";
            this.BtnInfoUpdate.Size = new System.Drawing.Size(136, 35);
            this.BtnInfoUpdate.TabIndex = 2;
            this.BtnInfoUpdate.Text = "报警信息更新";
            this.BtnInfoUpdate.Click += new System.EventHandler(this.BtnInfoUpdate_Click);
            // 
            // BtnInfoDown
            // 
            this.BtnInfoDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnInfoDown.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.BtnInfoDown.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.BtnInfoDown.ForeColor = System.Drawing.Color.Black;
            this.BtnInfoDown.Location = new System.Drawing.Point(1032, 608);
            this.BtnInfoDown.Name = "BtnInfoDown";
            this.BtnInfoDown.Size = new System.Drawing.Size(136, 35);
            this.BtnInfoDown.Style = Sunny.UI.UIStyle.Custom;
            this.BtnInfoDown.TabIndex = 3;
            this.BtnInfoDown.Text = "报警导出";
            this.BtnInfoDown.Click += new System.EventHandler(this.BtnDel_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FloralWhite;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(12, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 33);
            this.label1.TabIndex = 15;
            this.label1.Text = "序号";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FloralWhite;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(70, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(232, 33);
            this.label2.TabIndex = 16;
            this.label2.Text = "报警时间";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FloralWhite;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(308, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(696, 33);
            this.label3.TabIndex = 17;
            this.label3.Text = "报警信息";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // uiTextBox1
            // 
            this.uiTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox1.FillColor = System.Drawing.Color.White;
            this.uiTextBox1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox1.Location = new System.Drawing.Point(1032, 107);
            this.uiTextBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox1.Maximum = 2147483647D;
            this.uiTextBox1.Minimum = -2147483648D;
            this.uiTextBox1.Name = "uiTextBox1";
            this.uiTextBox1.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox1.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox1.TabIndex = 18;
            this.uiTextBox1.Tag = "0";
            this.uiTextBox1.Text = "uiTextBox1";
            // 
            // uiTextBox2
            // 
            this.uiTextBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox2.FillColor = System.Drawing.Color.White;
            this.uiTextBox2.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox2.Location = new System.Drawing.Point(1032, 156);
            this.uiTextBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox2.Maximum = 2147483647D;
            this.uiTextBox2.Minimum = -2147483648D;
            this.uiTextBox2.Name = "uiTextBox2";
            this.uiTextBox2.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox2.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox2.TabIndex = 19;
            this.uiTextBox2.Tag = "1";
            this.uiTextBox2.Text = "uiTextBox2";
            // 
            // uiTextBox3
            // 
            this.uiTextBox3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox3.FillColor = System.Drawing.Color.White;
            this.uiTextBox3.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox3.Location = new System.Drawing.Point(1032, 205);
            this.uiTextBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox3.Maximum = 2147483647D;
            this.uiTextBox3.Minimum = -2147483648D;
            this.uiTextBox3.Name = "uiTextBox3";
            this.uiTextBox3.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox3.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox3.TabIndex = 19;
            this.uiTextBox3.Tag = "2";
            this.uiTextBox3.Text = "uiTextBox3";
            // 
            // uiTextBox4
            // 
            this.uiTextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox4.FillColor = System.Drawing.Color.White;
            this.uiTextBox4.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox4.Location = new System.Drawing.Point(1032, 254);
            this.uiTextBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox4.Maximum = 2147483647D;
            this.uiTextBox4.Minimum = -2147483648D;
            this.uiTextBox4.Name = "uiTextBox4";
            this.uiTextBox4.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox4.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox4.TabIndex = 19;
            this.uiTextBox4.Tag = "3";
            this.uiTextBox4.Text = "uiTextBox4";
            // 
            // uiTextBox5
            // 
            this.uiTextBox5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox5.FillColor = System.Drawing.Color.White;
            this.uiTextBox5.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox5.Location = new System.Drawing.Point(1032, 303);
            this.uiTextBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox5.Maximum = 2147483647D;
            this.uiTextBox5.Minimum = -2147483648D;
            this.uiTextBox5.Name = "uiTextBox5";
            this.uiTextBox5.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox5.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox5.TabIndex = 19;
            this.uiTextBox5.Tag = "4";
            this.uiTextBox5.Text = "uiTextBox5";
            // 
            // uiTextBox6
            // 
            this.uiTextBox6.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox6.FillColor = System.Drawing.Color.White;
            this.uiTextBox6.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox6.Location = new System.Drawing.Point(1032, 352);
            this.uiTextBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox6.Maximum = 2147483647D;
            this.uiTextBox6.Minimum = -2147483648D;
            this.uiTextBox6.Name = "uiTextBox6";
            this.uiTextBox6.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox6.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox6.TabIndex = 20;
            this.uiTextBox6.Tag = "5";
            this.uiTextBox6.Text = "uiTextBox6";
            // 
            // uiTextBox7
            // 
            this.uiTextBox7.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox7.FillColor = System.Drawing.Color.White;
            this.uiTextBox7.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox7.Location = new System.Drawing.Point(1032, 401);
            this.uiTextBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox7.Maximum = 2147483647D;
            this.uiTextBox7.Minimum = -2147483648D;
            this.uiTextBox7.Name = "uiTextBox7";
            this.uiTextBox7.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox7.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox7.TabIndex = 20;
            this.uiTextBox7.Tag = "6";
            this.uiTextBox7.Text = "uiTextBox7";
            // 
            // uiTextBox8
            // 
            this.uiTextBox8.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox8.FillColor = System.Drawing.Color.White;
            this.uiTextBox8.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox8.Location = new System.Drawing.Point(1032, 450);
            this.uiTextBox8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox8.Maximum = 2147483647D;
            this.uiTextBox8.Minimum = -2147483648D;
            this.uiTextBox8.Name = "uiTextBox8";
            this.uiTextBox8.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox8.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox8.TabIndex = 21;
            this.uiTextBox8.Tag = "7";
            this.uiTextBox8.Text = "uiTextBox8";
            // 
            // uiTextBox9
            // 
            this.uiTextBox9.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox9.FillColor = System.Drawing.Color.White;
            this.uiTextBox9.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox9.Location = new System.Drawing.Point(1032, 499);
            this.uiTextBox9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox9.Maximum = 2147483647D;
            this.uiTextBox9.Minimum = -2147483648D;
            this.uiTextBox9.Name = "uiTextBox9";
            this.uiTextBox9.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox9.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox9.TabIndex = 21;
            this.uiTextBox9.Tag = "8";
            this.uiTextBox9.Text = "uiTextBox9";
            // 
            // uiTextBox10
            // 
            this.uiTextBox10.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.uiTextBox10.FillColor = System.Drawing.Color.White;
            this.uiTextBox10.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiTextBox10.Location = new System.Drawing.Point(1032, 548);
            this.uiTextBox10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uiTextBox10.Maximum = 2147483647D;
            this.uiTextBox10.Minimum = -2147483648D;
            this.uiTextBox10.Name = "uiTextBox10";
            this.uiTextBox10.Padding = new System.Windows.Forms.Padding(5);
            this.uiTextBox10.Size = new System.Drawing.Size(122, 34);
            this.uiTextBox10.TabIndex = 21;
            this.uiTextBox10.Tag = "9";
            this.uiTextBox10.Text = "uiTextBox10";
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmAlarmView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1212, 673);
            this.Controls.Add(this.uiTextBox10);
            this.Controls.Add(this.uiTextBox9);
            this.Controls.Add(this.uiTextBox8);
            this.Controls.Add(this.uiTextBox6);
            this.Controls.Add(this.uiTextBox7);
            this.Controls.Add(this.uiTextBox3);
            this.Controls.Add(this.uiTextBox2);
            this.Controls.Add(this.uiTextBox4);
            this.Controls.Add(this.uiTextBox5);
            this.Controls.Add(this.uiTextBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnInfoDown);
            this.Controls.Add(this.BtnInfoUpdate);
            this.Controls.Add(this.labTitle);
            this.Controls.Add(this.listAlarm);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmAlarmView";
            this.Text = "FrmAlarmView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAlarmView_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listAlarm;
        private System.Windows.Forms.Label labTitle;
        private Sunny.UI.UIButton BtnInfoUpdate;
        private Sunny.UI.UIButton BtnInfoDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Sunny.UI.UITextBox uiTextBox1;
        private Sunny.UI.UITextBox uiTextBox2;
        private Sunny.UI.UITextBox uiTextBox3;
        private Sunny.UI.UITextBox uiTextBox4;
        private Sunny.UI.UITextBox uiTextBox5;
        private Sunny.UI.UITextBox uiTextBox6;
        private Sunny.UI.UITextBox uiTextBox7;
        private Sunny.UI.UITextBox uiTextBox8;
        private Sunny.UI.UITextBox uiTextBox9;
        private Sunny.UI.UITextBox uiTextBox10;
        private System.Windows.Forms.Timer timer1;
    }
}