﻿namespace BinHmiSystem
{
    partial class FrmAlarmEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labTitle = new System.Windows.Forms.Label();
            this.DGWAlarmEdit = new System.Windows.Forms.DataGridView();
            this.VarName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VarAdress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VarType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.VarInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VarDel = new System.Windows.Forms.DataGridViewLinkColumn();
            this.btnOpen = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.btnSaveAs = new System.Windows.Forms.Button();
            this.btnDownLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGWAlarmEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // labTitle
            // 
            this.labTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTitle.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTitle.Location = new System.Drawing.Point(425, 6);
            this.labTitle.Name = "labTitle";
            this.labTitle.Size = new System.Drawing.Size(310, 39);
            this.labTitle.TabIndex = 20;
            this.labTitle.Text = "报警参数编辑";
            this.labTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DGWAlarmEdit
            // 
            this.DGWAlarmEdit.AllowUserToDeleteRows = false;
            this.DGWAlarmEdit.AllowUserToResizeColumns = false;
            this.DGWAlarmEdit.AllowUserToResizeRows = false;
            this.DGWAlarmEdit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGWAlarmEdit.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGWAlarmEdit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGWAlarmEdit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGWAlarmEdit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VarName,
            this.VarAdress,
            this.VarType,
            this.VarInfo,
            this.VarDel});
            this.DGWAlarmEdit.Location = new System.Drawing.Point(2, 97);
            this.DGWAlarmEdit.Name = "DGWAlarmEdit";
            this.DGWAlarmEdit.RowHeadersWidth = 15;
            this.DGWAlarmEdit.RowTemplate.Height = 27;
            this.DGWAlarmEdit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DGWAlarmEdit.Size = new System.Drawing.Size(1207, 571);
            this.DGWAlarmEdit.TabIndex = 21;
            this.DGWAlarmEdit.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGWAlarmEdit_CellContentClick);
            // 
            // VarName
            // 
            this.VarName.DataPropertyName = "VarName";
            this.VarName.FillWeight = 65.21507F;
            this.VarName.HeaderText = "变量名称";
            this.VarName.MinimumWidth = 6;
            this.VarName.Name = "VarName";
            // 
            // VarAdress
            // 
            this.VarAdress.DataPropertyName = "VarAddress";
            this.VarAdress.FillWeight = 55.21609F;
            this.VarAdress.HeaderText = "地址";
            this.VarAdress.MinimumWidth = 6;
            this.VarAdress.Name = "VarAdress";
            // 
            // VarType
            // 
            this.VarType.DataPropertyName = "Vartype";
            this.VarType.FillWeight = 53.47593F;
            this.VarType.HeaderText = "数据类型";
            this.VarType.MinimumWidth = 6;
            this.VarType.Name = "VarType";
            this.VarType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.VarType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // VarInfo
            // 
            this.VarInfo.DataPropertyName = "AlarmInfo";
            this.VarInfo.FillWeight = 269.5845F;
            this.VarInfo.HeaderText = "报警信息";
            this.VarInfo.MinimumWidth = 6;
            this.VarInfo.Name = "VarInfo";
            // 
            // VarDel
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = "删除";
            this.VarDel.DefaultCellStyle = dataGridViewCellStyle2;
            this.VarDel.FillWeight = 56.50835F;
            this.VarDel.HeaderText = "操作";
            this.VarDel.MinimumWidth = 6;
            this.VarDel.Name = "VarDel";
            // 
            // btnOpen
            // 
            this.btnOpen.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnOpen.Font = new System.Drawing.Font("宋体", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnOpen.Location = new System.Drawing.Point(580, 57);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(33, 25);
            this.btnOpen.TabIndex = 22;
            this.btnOpen.Text = "...";
            this.btnOpen.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnOpen.UseVisualStyleBackColor = false;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(13, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 25);
            this.label1.TabIndex = 24;
            this.label1.Text = "本地文件";
            // 
            // txtPath
            // 
            this.txtPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPath.Location = new System.Drawing.Point(108, 57);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(471, 25);
            this.txtPath.TabIndex = 25;
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Image = global::BinHmiSystem.Properties.Resources.另存为__2_;
            this.btnSaveAs.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveAs.Location = new System.Drawing.Point(873, 58);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(103, 27);
            this.btnSaveAs.TabIndex = 28;
            this.btnSaveAs.Text = " 另存为";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // btnDownLoad
            // 
            this.btnDownLoad.Image = global::BinHmiSystem.Properties.Resources.下载1;
            this.btnDownLoad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDownLoad.Location = new System.Drawing.Point(1078, 58);
            this.btnDownLoad.Name = "btnDownLoad";
            this.btnDownLoad.Size = new System.Drawing.Size(122, 27);
            this.btnDownLoad.TabIndex = 27;
            this.btnDownLoad.Text = " 下载到系统";
            this.btnDownLoad.UseVisualStyleBackColor = true;
            this.btnDownLoad.Click += new System.EventHandler(this.btnDownLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::BinHmiSystem.Properties.Resources.保存1;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(764, 57);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(103, 27);
            this.btnSave.TabIndex = 26;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpload
            // 
            this.btnUpload.Image = global::BinHmiSystem.Properties.Resources.上传1;
            this.btnUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpload.Location = new System.Drawing.Point(619, 57);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(99, 27);
            this.btnUpload.TabIndex = 23;
            this.btnUpload.Text = "  本地上传";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // FrmAlarmEdit
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1212, 673);
            this.Controls.Add(this.btnSaveAs);
            this.Controls.Add(this.btnDownLoad);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.DGWAlarmEdit);
            this.Controls.Add(this.labTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmAlarmEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmAlarmEdit";
            ((System.ComponentModel.ISupportInitialize)(this.DGWAlarmEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labTitle;
        private System.Windows.Forms.DataGridView DGWAlarmEdit;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDownLoad;
        private System.Windows.Forms.Button btnSaveAs;
        private System.Windows.Forms.DataGridViewTextBoxColumn VarName;
        private System.Windows.Forms.DataGridViewTextBoxColumn VarAdress;
        private System.Windows.Forms.DataGridViewComboBoxColumn VarType;
        private System.Windows.Forms.DataGridViewTextBoxColumn VarInfo;
        private System.Windows.Forms.DataGridViewLinkColumn VarDel;
    }
}