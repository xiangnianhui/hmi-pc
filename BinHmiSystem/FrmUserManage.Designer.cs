﻿namespace BinHmiSystem
{
    partial class FrmUserManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DWGuser = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userpwd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.authority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userdel = new System.Windows.Forms.DataGridViewLinkColumn();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.CBuserauthor = new Sunny.UI.UIComboBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtusername = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtuserpwd = new System.Windows.Forms.TextBox();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.CBalterauthor = new Sunny.UI.UIComboBox();
            this.btnAlter = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtalterpwd = new System.Windows.Forms.TextBox();
            this.txtaltername = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DWGuser)).BeginInit();
            this.metroPanel1.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // DWGuser
            // 
            this.DWGuser.AllowUserToDeleteRows = false;
            this.DWGuser.AllowUserToResizeColumns = false;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.DWGuser.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.DWGuser.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DWGuser.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DWGuser.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.DWGuser.ColumnHeadersHeight = 33;
            this.DWGuser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DWGuser.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.username,
            this.userpwd,
            this.authority,
            this.userdel});
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DWGuser.DefaultCellStyle = dataGridViewCellStyle32;
            this.DWGuser.Dock = System.Windows.Forms.DockStyle.Left;
            this.DWGuser.GridColor = System.Drawing.Color.LightGray;
            this.DWGuser.Location = new System.Drawing.Point(0, 0);
            this.DWGuser.MultiSelect = false;
            this.DWGuser.Name = "DWGuser";
            this.DWGuser.ReadOnly = true;
            this.DWGuser.RowHeadersWidth = 51;
            this.DWGuser.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DWGuser.RowTemplate.Height = 27;
            this.DWGuser.Size = new System.Drawing.Size(572, 667);
            this.DWGuser.TabIndex = 0;
            this.DWGuser.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DWGuser_CellContentClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ID.DefaultCellStyle = dataGridViewCellStyle27;
            this.ID.FillWeight = 75F;
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // username
            // 
            this.username.DataPropertyName = "Name";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.username.DefaultCellStyle = dataGridViewCellStyle28;
            this.username.HeaderText = "用户名";
            this.username.MinimumWidth = 6;
            this.username.Name = "username";
            this.username.ReadOnly = true;
            // 
            // userpwd
            // 
            this.userpwd.DataPropertyName = "PWD";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.userpwd.DefaultCellStyle = dataGridViewCellStyle29;
            this.userpwd.HeaderText = "用户密码";
            this.userpwd.MinimumWidth = 6;
            this.userpwd.Name = "userpwd";
            this.userpwd.ReadOnly = true;
            // 
            // authority
            // 
            this.authority.DataPropertyName = "Author";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.authority.DefaultCellStyle = dataGridViewCellStyle30;
            this.authority.HeaderText = "用户权限";
            this.authority.MinimumWidth = 6;
            this.authority.Name = "authority";
            this.authority.ReadOnly = true;
            // 
            // userdel
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.NullValue = "删除";
            this.userdel.DefaultCellStyle = dataGridViewCellStyle31;
            this.userdel.HeaderText = "操作";
            this.userdel.LinkColor = System.Drawing.Color.Black;
            this.userdel.MinimumWidth = 6;
            this.userdel.Name = "userdel";
            this.userdel.ReadOnly = true;
            this.userdel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.userdel.Text = "删除";
            this.userdel.UseColumnTextForLinkValue = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.metroPanel1.Controls.Add(this.CBuserauthor);
            this.metroPanel1.Controls.Add(this.btnAdd);
            this.metroPanel1.Controls.Add(this.label5);
            this.metroPanel1.Controls.Add(this.label6);
            this.metroPanel1.Controls.Add(this.label7);
            this.metroPanel1.Controls.Add(this.txtusername);
            this.metroPanel1.Controls.Add(this.label8);
            this.metroPanel1.Controls.Add(this.txtuserpwd);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(578, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(632, 330);
            this.metroPanel1.TabIndex = 1;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // CBuserauthor
            // 
            this.CBuserauthor.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            this.CBuserauthor.FillColor = System.Drawing.Color.White;
            this.CBuserauthor.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.CBuserauthor.Location = new System.Drawing.Point(263, 200);
            this.CBuserauthor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CBuserauthor.MinimumSize = new System.Drawing.Size(63, 0);
            this.CBuserauthor.Name = "CBuserauthor";
            this.CBuserauthor.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.CBuserauthor.RectColor = System.Drawing.Color.Gray;
            this.CBuserauthor.Size = new System.Drawing.Size(198, 30);
            this.CBuserauthor.Style = Sunny.UI.UIStyle.Custom;
            this.CBuserauthor.TabIndex = 3;
            this.CBuserauthor.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Aquamarine;
            this.btnAdd.Font = new System.Drawing.Font("微软雅黑", 10.8F);
            this.btnAdd.Location = new System.Drawing.Point(262, 257);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(199, 39);
            this.btnAdd.TabIndex = 17;
            this.btnAdd.Text = "新建";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(299, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 27);
            this.label5.TabIndex = 16;
            this.label5.Text = "账号创建";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(204, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 27);
            this.label6.TabIndex = 15;
            this.label6.Text = "权限";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(204, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 27);
            this.label7.TabIndex = 14;
            this.label7.Text = "密码";
            // 
            // txtusername
            // 
            this.txtusername.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtusername.Location = new System.Drawing.Point(262, 77);
            this.txtusername.Name = "txtusername";
            this.txtusername.Size = new System.Drawing.Size(199, 30);
            this.txtusername.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(204, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 27);
            this.label8.TabIndex = 13;
            this.label8.Text = "账号";
            // 
            // txtuserpwd
            // 
            this.txtuserpwd.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtuserpwd.Location = new System.Drawing.Point(262, 138);
            this.txtuserpwd.Name = "txtuserpwd";
            this.txtuserpwd.Size = new System.Drawing.Size(199, 30);
            this.txtuserpwd.TabIndex = 11;
            // 
            // metroPanel2
            // 
            this.metroPanel2.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.metroPanel2.Controls.Add(this.CBalterauthor);
            this.metroPanel2.Controls.Add(this.btnAlter);
            this.metroPanel2.Controls.Add(this.label4);
            this.metroPanel2.Controls.Add(this.label3);
            this.metroPanel2.Controls.Add(this.label2);
            this.metroPanel2.Controls.Add(this.label1);
            this.metroPanel2.Controls.Add(this.txtalterpwd);
            this.metroPanel2.Controls.Add(this.txtaltername);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(578, 337);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(632, 330);
            this.metroPanel2.TabIndex = 2;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // CBalterauthor
            // 
            this.CBalterauthor.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            this.CBalterauthor.FillColor = System.Drawing.Color.White;
            this.CBalterauthor.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.CBalterauthor.Location = new System.Drawing.Point(263, 195);
            this.CBalterauthor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CBalterauthor.MinimumSize = new System.Drawing.Size(63, 0);
            this.CBalterauthor.Name = "CBalterauthor";
            this.CBalterauthor.Padding = new System.Windows.Forms.Padding(0, 0, 30, 0);
            this.CBalterauthor.RectColor = System.Drawing.Color.Gray;
            this.CBalterauthor.Size = new System.Drawing.Size(198, 31);
            this.CBalterauthor.Style = Sunny.UI.UIStyle.Custom;
            this.CBalterauthor.TabIndex = 4;
            this.CBalterauthor.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAlter
            // 
            this.btnAlter.BackColor = System.Drawing.Color.PeachPuff;
            this.btnAlter.Font = new System.Drawing.Font("微软雅黑", 10.8F);
            this.btnAlter.Location = new System.Drawing.Point(262, 252);
            this.btnAlter.Name = "btnAlter";
            this.btnAlter.Size = new System.Drawing.Size(199, 39);
            this.btnAlter.TabIndex = 9;
            this.btnAlter.Text = "修改";
            this.btnAlter.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(299, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 27);
            this.label4.TabIndex = 8;
            this.label4.Text = "账号修改";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(204, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 27);
            this.label3.TabIndex = 7;
            this.label3.Text = "权限";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(204, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 27);
            this.label2.TabIndex = 6;
            this.label2.Text = "密码";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(204, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 27);
            this.label1.TabIndex = 5;
            this.label1.Text = "账号";
            // 
            // txtalterpwd
            // 
            this.txtalterpwd.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtalterpwd.Location = new System.Drawing.Point(262, 133);
            this.txtalterpwd.Name = "txtalterpwd";
            this.txtalterpwd.Size = new System.Drawing.Size(199, 30);
            this.txtalterpwd.TabIndex = 3;
            // 
            // txtaltername
            // 
            this.txtaltername.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtaltername.Location = new System.Drawing.Point(262, 72);
            this.txtaltername.Name = "txtaltername";
            this.txtaltername.Size = new System.Drawing.Size(199, 30);
            this.txtaltername.TabIndex = 2;
            // 
            // FrmUserManage
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1212, 667);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.DWGuser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmUserManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmUserManage";
            ((System.ComponentModel.ISupportInitialize)(this.DWGuser)).EndInit();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DWGuser;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private System.Windows.Forms.TextBox txtalterpwd;
        private System.Windows.Forms.TextBox txtaltername;
        private System.Windows.Forms.Button btnAlter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtusername;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtuserpwd;
        private Sunny.UI.UIComboBox CBuserauthor;
        private Sunny.UI.UIComboBox CBalterauthor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn username;
        private System.Windows.Forms.DataGridViewTextBoxColumn userpwd;
        private System.Windows.Forms.DataGridViewTextBoxColumn authority;
        private System.Windows.Forms.DataGridViewLinkColumn userdel;
    }
}