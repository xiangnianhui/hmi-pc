﻿namespace BinHmiSystem
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.miniToolStrip = new System.Windows.Forms.MenuStrip();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labhome = new System.Windows.Forms.Label();
            this.BtnHome = new Sunny.UI.UIImageButton();
            this.labstop = new System.Windows.Forms.Label();
            this.labstart = new System.Windows.Forms.Label();
            this.BtnStop = new Sunny.UI.UIImageButton();
            this.BtnStart = new Sunny.UI.UIImageButton();
            this.Panelcontent = new System.Windows.Forms.Panel();
            this.PanelMS = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.用户登录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户登陆ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户管理ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.程序ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.新建程序ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.下载配方ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.监视ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.警报ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.实时报警ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.历史报警ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.报警设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Panel = new System.Windows.Forms.Panel();
            this.labPlcConState = new System.Windows.Forms.Label();
            this.PanelTitle = new System.Windows.Forms.Panel();
            this.BtnLoginOut = new System.Windows.Forms.Button();
            this.labuser = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnWindowsMax = new System.Windows.Forms.Button();
            this.BtnFClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LedConnectState = new UIDesgin.UIComponent.LED();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BtnHome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnStart)).BeginInit();
            this.PanelMS.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.Panel.SuspendLayout();
            this.PanelTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AccessibleName = "新项选择";
            this.miniToolStrip.AccessibleRole = System.Windows.Forms.AccessibleRole.ComboBox;
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.miniToolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.miniToolStrip.Location = new System.Drawing.Point(1066, 11);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.miniToolStrip.Size = new System.Drawing.Size(1348, 29);
            this.miniToolStrip.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.labhome);
            this.panel1.Controls.Add(this.BtnHome);
            this.panel1.Controls.Add(this.labstop);
            this.panel1.Controls.Add(this.labstart);
            this.panel1.Controls.Add(this.BtnStop);
            this.panel1.Controls.Add(this.BtnStart);
            this.panel1.Controls.Add(this.Panelcontent);
            this.panel1.Controls.Add(this.PanelMS);
            this.panel1.Controls.Add(this.Panel);
            this.panel1.Controls.Add(this.PanelTitle);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1280, 800);
            this.panel1.TabIndex = 0;
            // 
            // labhome
            // 
            this.labhome.BackColor = System.Drawing.Color.Transparent;
            this.labhome.Location = new System.Drawing.Point(6, 214);
            this.labhome.Name = "labhome";
            this.labhome.Size = new System.Drawing.Size(57, 23);
            this.labhome.TabIndex = 11;
            this.labhome.Text = "HOME";
            this.labhome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnHome
            // 
            this.BtnHome.BackColor = System.Drawing.Color.Transparent;
            this.BtnHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnHome.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.BtnHome.Image = global::BinHmiSystem.Properties.Resources.主页;
            this.BtnHome.ImagePress = global::BinHmiSystem.Properties.Resources.主页__1_;
            this.BtnHome.Location = new System.Drawing.Point(5, 157);
            this.BtnHome.Name = "BtnHome";
            this.BtnHome.Size = new System.Drawing.Size(56, 53);
            this.BtnHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BtnHome.TabIndex = 10;
            this.BtnHome.TabStop = false;
            this.BtnHome.Tag = "1";
            this.BtnHome.Text = null;
            this.BtnHome.Click += new System.EventHandler(this.BtnHome_Click);
            // 
            // labstop
            // 
            this.labstop.BackColor = System.Drawing.Color.Transparent;
            this.labstop.Location = new System.Drawing.Point(5, 494);
            this.labstop.Name = "labstop";
            this.labstop.Size = new System.Drawing.Size(57, 23);
            this.labstop.TabIndex = 9;
            this.labstop.Text = "STOP";
            this.labstop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labstart
            // 
            this.labstart.BackColor = System.Drawing.Color.Transparent;
            this.labstart.Location = new System.Drawing.Point(5, 351);
            this.labstart.Name = "labstart";
            this.labstart.Size = new System.Drawing.Size(57, 23);
            this.labstart.TabIndex = 8;
            this.labstart.Text = "START";
            this.labstart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnStop
            // 
            this.BtnStop.BackColor = System.Drawing.Color.Transparent;
            this.BtnStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnStop.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.BtnStop.Image = global::BinHmiSystem.Properties.Resources.停止;
            this.BtnStop.ImagePress = global::BinHmiSystem.Properties.Resources.停止__2_;
            this.BtnStop.Location = new System.Drawing.Point(4, 437);
            this.BtnStop.Name = "BtnStop";
            this.BtnStop.Size = new System.Drawing.Size(56, 53);
            this.BtnStop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BtnStop.TabIndex = 7;
            this.BtnStop.TabStop = false;
            this.BtnStop.Tag = "2";
            this.BtnStop.Text = null;
            // 
            // BtnStart
            // 
            this.BtnStart.BackColor = System.Drawing.Color.Transparent;
            this.BtnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnStart.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.BtnStart.Image = global::BinHmiSystem.Properties.Resources.启动;
            this.BtnStart.ImagePress = global::BinHmiSystem.Properties.Resources.启动__1_;
            this.BtnStart.Location = new System.Drawing.Point(5, 294);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(56, 53);
            this.BtnStart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BtnStart.TabIndex = 6;
            this.BtnStart.TabStop = false;
            this.BtnStart.Tag = "1";
            this.BtnStart.Text = null;
            // 
            // Panelcontent
            // 
            this.Panelcontent.BackColor = System.Drawing.Color.White;
            this.Panelcontent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Panelcontent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panelcontent.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panelcontent.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Panelcontent.Location = new System.Drawing.Point(66, 91);
            this.Panelcontent.Margin = new System.Windows.Forms.Padding(0);
            this.Panelcontent.Name = "Panelcontent";
            this.Panelcontent.Size = new System.Drawing.Size(1212, 673);
            this.Panelcontent.TabIndex = 4;
            // 
            // PanelMS
            // 
            this.PanelMS.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.PanelMS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelMS.Controls.Add(this.menuStrip1);
            this.PanelMS.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelMS.Location = new System.Drawing.Point(0, 60);
            this.PanelMS.Name = "PanelMS";
            this.PanelMS.Size = new System.Drawing.Size(1278, 31);
            this.PanelMS.TabIndex = 3;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.用户登录ToolStripMenuItem,
            this.程序ToolStripMenuItem,
            this.用户管理ToolStripMenuItem,
            this.数据ToolStripMenuItem,
            this.监视ToolStripMenuItem,
            this.警报ToolStripMenuItem,
            this.设置ToolStripMenuItem,
            this.关于ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.menuStrip1.Size = new System.Drawing.Size(1276, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 用户登录ToolStripMenuItem
            // 
            this.用户登录ToolStripMenuItem.AutoSize = false;
            this.用户登录ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.用户登陆ToolStripMenuItem,
            this.用户管理ToolStripMenuItem1});
            this.用户登录ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.用户登录ToolStripMenuItem.Image = global::BinHmiSystem.Properties.Resources.用户;
            this.用户登录ToolStripMenuItem.Name = "用户登录ToolStripMenuItem";
            this.用户登录ToolStripMenuItem.Size = new System.Drawing.Size(120, 30);
            this.用户登录ToolStripMenuItem.Text = "用户 ";
            // 
            // 用户登陆ToolStripMenuItem
            // 
            this.用户登陆ToolStripMenuItem.Name = "用户登陆ToolStripMenuItem";
            this.用户登陆ToolStripMenuItem.Size = new System.Drawing.Size(178, 32);
            this.用户登陆ToolStripMenuItem.Text = "用户登陆";
            this.用户登陆ToolStripMenuItem.Click += new System.EventHandler(this.用户登陆ToolStripMenuItem_Click);
            // 
            // 用户管理ToolStripMenuItem1
            // 
            this.用户管理ToolStripMenuItem1.Name = "用户管理ToolStripMenuItem1";
            this.用户管理ToolStripMenuItem1.Size = new System.Drawing.Size(178, 32);
            this.用户管理ToolStripMenuItem1.Text = "用户管理";
            this.用户管理ToolStripMenuItem1.Click += new System.EventHandler(this.用户管理ToolStripMenuItem1_Click);
            // 
            // 程序ToolStripMenuItem
            // 
            this.程序ToolStripMenuItem.AutoSize = false;
            this.程序ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.新建程序ToolStripMenuItem,
            this.下载配方ToolStripMenuItem});
            this.程序ToolStripMenuItem.Image = global::BinHmiSystem.Properties.Resources.文档;
            this.程序ToolStripMenuItem.Name = "程序ToolStripMenuItem";
            this.程序ToolStripMenuItem.Size = new System.Drawing.Size(120, 30);
            this.程序ToolStripMenuItem.Text = "配方 ";
            // 
            // 新建程序ToolStripMenuItem
            // 
            this.新建程序ToolStripMenuItem.Name = "新建程序ToolStripMenuItem";
            this.新建程序ToolStripMenuItem.Size = new System.Drawing.Size(178, 32);
            this.新建程序ToolStripMenuItem.Text = "创建配方";
            this.新建程序ToolStripMenuItem.Click += new System.EventHandler(this.新建程序ToolStripMenuItem_Click);
            // 
            // 下载配方ToolStripMenuItem
            // 
            this.下载配方ToolStripMenuItem.Name = "下载配方ToolStripMenuItem";
            this.下载配方ToolStripMenuItem.Size = new System.Drawing.Size(178, 32);
            this.下载配方ToolStripMenuItem.Text = "下载配方";
            this.下载配方ToolStripMenuItem.Click += new System.EventHandler(this.下载配方ToolStripMenuItem_Click);
            // 
            // 用户管理ToolStripMenuItem
            // 
            this.用户管理ToolStripMenuItem.AutoSize = false;
            this.用户管理ToolStripMenuItem.Image = global::BinHmiSystem.Properties.Resources.手动同步;
            this.用户管理ToolStripMenuItem.Name = "用户管理ToolStripMenuItem";
            this.用户管理ToolStripMenuItem.Size = new System.Drawing.Size(120, 30);
            this.用户管理ToolStripMenuItem.Text = "手动 ";
            // 
            // 数据ToolStripMenuItem
            // 
            this.数据ToolStripMenuItem.AutoSize = false;
            this.数据ToolStripMenuItem.Image = global::BinHmiSystem.Properties.Resources.数据;
            this.数据ToolStripMenuItem.Name = "数据ToolStripMenuItem";
            this.数据ToolStripMenuItem.Size = new System.Drawing.Size(120, 30);
            this.数据ToolStripMenuItem.Text = "数据 ";
            // 
            // 监视ToolStripMenuItem
            // 
            this.监视ToolStripMenuItem.AutoSize = false;
            this.监视ToolStripMenuItem.Image = global::BinHmiSystem.Properties.Resources.监视;
            this.监视ToolStripMenuItem.Name = "监视ToolStripMenuItem";
            this.监视ToolStripMenuItem.Size = new System.Drawing.Size(120, 30);
            this.监视ToolStripMenuItem.Text = "监视 ";
            // 
            // 警报ToolStripMenuItem
            // 
            this.警报ToolStripMenuItem.AutoSize = false;
            this.警报ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.实时报警ToolStripMenuItem,
            this.历史报警ToolStripMenuItem,
            this.报警设置ToolStripMenuItem});
            this.警报ToolStripMenuItem.Image = global::BinHmiSystem.Properties.Resources.报警;
            this.警报ToolStripMenuItem.Name = "警报ToolStripMenuItem";
            this.警报ToolStripMenuItem.Size = new System.Drawing.Size(120, 30);
            this.警报ToolStripMenuItem.Text = "警报 ";
            // 
            // 实时报警ToolStripMenuItem
            // 
            this.实时报警ToolStripMenuItem.Name = "实时报警ToolStripMenuItem";
            this.实时报警ToolStripMenuItem.Size = new System.Drawing.Size(224, 32);
            this.实时报警ToolStripMenuItem.Text = "实时报警";
            this.实时报警ToolStripMenuItem.Click += new System.EventHandler(this.实时报警ToolStripMenuItem_Click);
            // 
            // 历史报警ToolStripMenuItem
            // 
            this.历史报警ToolStripMenuItem.Name = "历史报警ToolStripMenuItem";
            this.历史报警ToolStripMenuItem.Size = new System.Drawing.Size(224, 32);
            this.历史报警ToolStripMenuItem.Text = "历史报警";
            this.历史报警ToolStripMenuItem.Click += new System.EventHandler(this.历史报警ToolStripMenuItem_Click);
            // 
            // 报警设置ToolStripMenuItem
            // 
            this.报警设置ToolStripMenuItem.Name = "报警设置ToolStripMenuItem";
            this.报警设置ToolStripMenuItem.Size = new System.Drawing.Size(224, 32);
            this.报警设置ToolStripMenuItem.Text = "报警设置";
            this.报警设置ToolStripMenuItem.Click += new System.EventHandler(this.报警设置ToolStripMenuItem_Click);
            // 
            // 设置ToolStripMenuItem
            // 
            this.设置ToolStripMenuItem.AutoSize = false;
            this.设置ToolStripMenuItem.Image = global::BinHmiSystem.Properties.Resources.设_置字段;
            this.设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            this.设置ToolStripMenuItem.Size = new System.Drawing.Size(120, 30);
            this.设置ToolStripMenuItem.Text = "设置 ";
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.AutoSize = false;
            this.关于ToolStripMenuItem.Image = global::BinHmiSystem.Properties.Resources.关于;
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(120, 30);
            this.关于ToolStripMenuItem.Text = "关于 ";
            // 
            // Panel
            // 
            this.Panel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel.Controls.Add(this.labPlcConState);
            this.Panel.Controls.Add(this.LedConnectState);
            this.Panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel.Location = new System.Drawing.Point(0, 764);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(1278, 34);
            this.Panel.TabIndex = 1;
            // 
            // labPlcConState
            // 
            this.labPlcConState.AutoSize = true;
            this.labPlcConState.Font = new System.Drawing.Font("微软雅黑", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labPlcConState.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labPlcConState.Location = new System.Drawing.Point(1146, 4);
            this.labPlcConState.Name = "labPlcConState";
            this.labPlcConState.Size = new System.Drawing.Size(88, 25);
            this.labPlcConState.TabIndex = 1;
            this.labPlcConState.Text = "通讯状态";
            // 
            // PanelTitle
            // 
            this.PanelTitle.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.PanelTitle.BackgroundImage = global::BinHmiSystem.Properties.Resources.BG1;
            this.PanelTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelTitle.Controls.Add(this.BtnLoginOut);
            this.PanelTitle.Controls.Add(this.labuser);
            this.PanelTitle.Controls.Add(this.label2);
            this.PanelTitle.Controls.Add(this.BtnWindowsMax);
            this.PanelTitle.Controls.Add(this.BtnFClose);
            this.PanelTitle.Controls.Add(this.label1);
            this.PanelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTitle.Location = new System.Drawing.Point(0, 0);
            this.PanelTitle.Name = "PanelTitle";
            this.PanelTitle.Size = new System.Drawing.Size(1278, 60);
            this.PanelTitle.TabIndex = 0;
            this.PanelTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.PanelTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.PanelTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // BtnLoginOut
            // 
            this.BtnLoginOut.AutoSize = true;
            this.BtnLoginOut.BackColor = System.Drawing.Color.LightSalmon;
            this.BtnLoginOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnLoginOut.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnLoginOut.Location = new System.Drawing.Point(1111, 11);
            this.BtnLoginOut.Name = "BtnLoginOut";
            this.BtnLoginOut.Size = new System.Drawing.Size(64, 31);
            this.BtnLoginOut.TabIndex = 5;
            this.BtnLoginOut.Text = "注销";
            this.BtnLoginOut.UseVisualStyleBackColor = false;
            this.BtnLoginOut.Click += new System.EventHandler(this.BtnLoginOut_Click);
            // 
            // labuser
            // 
            this.labuser.AutoSize = true;
            this.labuser.BackColor = System.Drawing.Color.Transparent;
            this.labuser.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labuser.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labuser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labuser.Location = new System.Drawing.Point(1032, 14);
            this.labuser.Name = "labuser";
            this.labuser.Size = new System.Drawing.Size(50, 27);
            this.labuser.TabIndex = 4;
            this.labuser.Text = "null";
            this.labuser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(936, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "当前用户:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnWindowsMax
            // 
            this.BtnWindowsMax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(132)))), ((int)(((byte)(254)))));
            this.BtnWindowsMax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnWindowsMax.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnWindowsMax.ForeColor = System.Drawing.Color.Transparent;
            this.BtnWindowsMax.Location = new System.Drawing.Point(1207, 3);
            this.BtnWindowsMax.Name = "BtnWindowsMax";
            this.BtnWindowsMax.Size = new System.Drawing.Size(30, 30);
            this.BtnWindowsMax.TabIndex = 2;
            this.BtnWindowsMax.Text = "口";
            this.BtnWindowsMax.UseVisualStyleBackColor = false;
            this.BtnWindowsMax.Visible = false;
            this.BtnWindowsMax.Click += new System.EventHandler(this.BtnWindowsMax_Click);
            // 
            // BtnFClose
            // 
            this.BtnFClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(18)))), ((int)(((byte)(132)))), ((int)(((byte)(254)))));
            this.BtnFClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFClose.Font = new System.Drawing.Font("微软雅黑", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnFClose.ForeColor = System.Drawing.Color.Transparent;
            this.BtnFClose.Location = new System.Drawing.Point(1243, 3);
            this.BtnFClose.Name = "BtnFClose";
            this.BtnFClose.Size = new System.Drawing.Size(30, 30);
            this.BtnFClose.TabIndex = 1;
            this.BtnFClose.Text = "X";
            this.BtnFClose.UseVisualStyleBackColor = false;
            this.BtnFClose.Click += new System.EventHandler(this.BtnFClose_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Highlight;
            this.label1.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(40, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(323, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "CMS管理系统";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LedConnectState
            // 
            this.LedConnectState.BoderWidth = 3;
            this.LedConnectState.bValue = false;
            this.LedConnectState.GapWidth = 0;
            this.LedConnectState.IsBoder = true;
            this.LedConnectState.LedColor = System.Drawing.Color.LimeGreen;
            this.LedConnectState.LedflaseColor = System.Drawing.Color.DimGray;
            this.LedConnectState.Location = new System.Drawing.Point(1236, 1);
            this.LedConnectState.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LedConnectState.Name = "LedConnectState";
            this.LedConnectState.Size = new System.Drawing.Size(27, 27);
            this.LedConnectState.TabIndex = 0;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1280, 800);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.miniToolStrip;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " C#上位机";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BtnHome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnStart)).EndInit();
            this.PanelMS.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.Panel.ResumeLayout(false);
            this.Panel.PerformLayout();
            this.PanelTitle.ResumeLayout(false);
            this.PanelTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelTitle;
        private System.Windows.Forms.Button BtnFClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.Panel PanelMS;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 用户登录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户登陆ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户管理ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 程序ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 监视ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.MenuStrip miniToolStrip;
        private System.Windows.Forms.Panel Panelcontent;
        private System.Windows.Forms.Panel panel1;
        private Sunny.UI.UIImageButton BtnStart;
        private System.Windows.Forms.Label labstop;
        private System.Windows.Forms.Label labstart;
        private Sunny.UI.UIImageButton BtnStop;
        private System.Windows.Forms.ToolStripMenuItem 新建程序ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 下载配方ToolStripMenuItem;
        private System.Windows.Forms.Label labhome;
        private Sunny.UI.UIImageButton BtnHome;
        private System.Windows.Forms.Button BtnWindowsMax;
        private System.Windows.Forms.Label labuser;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem 警报ToolStripMenuItem;
        private System.Windows.Forms.Label labPlcConState;
        private UIDesgin.UIComponent.LED LedConnectState;
        private System.Windows.Forms.ToolStripMenuItem 实时报警ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 历史报警ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 报警设置ToolStripMenuItem;
        private System.Windows.Forms.Button BtnLoginOut;
    }
}

