﻿using DAL;
using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BinHmiSystem
{
  
    public partial class FrmRecipeLoad : Form
    {

        public delegate void RecipDownAsc(List<PlcVarValue> varValues);//异步下载
        public RecipDownAsc up;


       public Plcservice plcservice;
        
        //配方文本
        string recipes ;
        //配方集合
        List<string> recipepath = new List<string>();
        //文件几个
        List<FileS> Filespath = new List<FileS>();
        //plc变量集合
        List<PlcVarValue> plcVars = new List<PlcVarValue>();
        //系统定时器
        System.Timers.Timer BarTimer;


        //窗体初始化
        public FrmRecipeLoad()
        {
            InitializeComponent();
            //自动创建列 false
            DGVRecipe.AutoGenerateColumns = false;
            DGVPlcVar.AutoGenerateColumns = false;
            //plc预览面板隐藏
            panel1.Visible = false;
            uiProcessBar1.Visible = false;
            #region 系统定时器的初始化
            BarTimer = new System.Timers.Timer();
            BarTimer.Interval = 2000;
            BarTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.BarTime_Tick);
            BarTimer.SynchronizingObject = this;
            #endregion
            DataInit();
        }
        //数据初始化
        private void DataInit()
        {
            //列表初始化
            recipepath.Clear();
            Filespath.Clear();
            recipes = null;
            //找到所有的配方目录
            FileHelper.FileFindAll(@"Recipe", ref recipes);
            //程序目录列表
            recipepath.AddRange(recipes.Split('\n'));
            //把目录列表转化成文件类列表
            for (int i = 0; i < recipepath.Count - 1; i++)
            {
                FileS file = new FileS();
                file.Name = recipepath[i];
                Filespath.Add(file);
            }
            DGVRecipe.DataSource = null;
            DGVRecipe.DataSource = Filespath;
        }
        //数据表格点击
        private void DGVRecipe_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //判断是否点击在有效范围
            if (e.RowIndex<0)
            {
                return;
            }
            //路径
            string path;
            //当前点击的格子
            DataGridViewCell downloadcell = DGVRecipe.Rows[e.RowIndex].Cells[e.ColumnIndex];
            if (downloadcell.FormattedValue.ToString() == "下载")
            {
                //找到路径
                path = DGVRecipe.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                if (DialogResult.OK == MessageBox.Show($"是否下载 {path}", "提示", MessageBoxButtons.OKCancel))
                {
                    //获取配方数据
                    plcVars = SerializeHelper<PlcVarValue>.XmlSerializeRead(@"Recipe\" + path);
                    //判断PLC是否存在
                    if (plcservice != null)
                    {
                        //显示下载进度条
                        uiProcessBar1.Visible = true;
                        //委托挂接刀进度条更新
                        plcservice.downloadprocess = processbarview;
                        //判断是都连接上
                        if (plcservice.IsConnected)
                        {//委托plc配方写入方法
                           up = new RecipDownAsc(plcservice.WriteRecipeData);
                            //异步执行plc配方写入
                           IAsyncResult ar= up.BeginInvoke(plcVars, CallBack, null);
                        }
                    }
                }
                return;
            }
            DataGridViewCell deletecell = DGVRecipe.Rows[e.RowIndex].Cells[e.ColumnIndex];
            if (deletecell.FormattedValue.ToString() == "删除")
            {
                path = DGVRecipe.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                if (DialogResult.OK==MessageBox.Show($"是否删除 {path}","提示",MessageBoxButtons.OKCancel))
                {
                    if (FileHelper.FileDel(@"Recipe\" + path))
                    {
                        DataInit();
                    }
                }
                return;
            }

            DataGridViewCell viewcell = DGVRecipe.Rows[e.RowIndex].Cells[e.ColumnIndex];
            if (viewcell.FormattedValue.ToString() == "预览")
            {
                //找到路径
                path = DGVRecipe.Rows[e.RowIndex].Cells[0].FormattedValue.ToString();
                //预览plc配方文件
                plcVars = SerializeHelper<PlcVarValue>.XmlSerializeRead(@"Recipe\"+path);
                DGVPlcVar.DataSource = null;
                DGVPlcVar.DataSource = plcVars;
                panel1.Visible = true;
                return;
            }

        }
        //窗体导入
        private void FrmRecipeLoad_Load(object sender, EventArgs e)
        {
           
        }
        //关闭plc变量预览窗口
        private void BtnDGVplcVarClose_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }
        //进度条更新
        private  void processbarview(int currentvalue, int maxvalue) 
        {
            uiProcessBar1.Maximum = maxvalue;
            uiProcessBar1.Value = currentvalue;

        }
        //异步下载更新回调函数
        private void CallBack(IAsyncResult result)
        {
            Console.WriteLine("回调函数正在执行");
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            up.EndInvoke(result);
            Console.WriteLine("异步现场清理完成");
            Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
            BarTimer.Stop();
            BarTimer.Start();
        }
        //定时关闭进度条
        private void BarTime_Tick(object sender, EventArgs e)
        {
            uiProcessBar1.Visible = false;
            BarTimer.Stop();
        }

    }
}
