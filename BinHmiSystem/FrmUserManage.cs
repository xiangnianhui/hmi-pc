﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using Models;

namespace BinHmiSystem
{
    public partial class FrmUserManage : Form
    {
        Userservice userservice = null;
        //初始化窗口
        public FrmUserManage()
        {
            InitializeComponent();
            userservice = new Userservice();
            DataBinding();
            DWGuser.AutoGenerateColumns = false;
            CBalterauthor.Items.AddRange( Enum.GetNames(typeof(Authority)));
            CBuserauthor.Items.AddRange(Enum.GetNames(typeof(Authority)));
            CBuserauthor.SelectedIndex = 0;
        }
        //数据表格数据绑定
        private void DataBinding()
        {
            userservice.Users = SerializeHelper<User>.BinarySerialzeRead("用户信息");
            DWGuser.DataSource = null;
            DWGuser.DataSource = userservice.Users;
        }
        //数据表格点击内容发生
        private void DWGuser_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex>=0)
            {
                txtaltername.Text = DWGuser.Rows[e.RowIndex].Cells["username"].Value.ToString();
                txtalterpwd.Text= DWGuser.Rows[e.RowIndex].Cells["userpwd"].Value.ToString();
                CBalterauthor.Text =DWGuser.Rows[e.RowIndex].Cells["authority"].Value.ToString();
                DataGridViewCell cell = DWGuser.Rows[e.RowIndex].Cells[e.ColumnIndex];
                if (cell.FormattedValue.ToString()=="删除")
                {
                    if (DialogResult.Yes== MessageBox.Show("是否删除", "提示", MessageBoxButtons.YesNo))
                    {
                        userservice.Users.RemoveAt(e.RowIndex);
                        SerializeHelper<User>.BinarySerialzeWrite("用户信息", userservice.Users);
                        DataBinding();
                    }  
                }
            }
        }
        //新建用户
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtusername.Text.Trim()==null)
            {
                MessageBox.Show("用户名不能为空");
                return;
            }
            if (txtuserpwd.Text == null)
            {
                MessageBox.Show("用户密码不能为空");
                return;
            }
            User newuser = new User();
            newuser.Name = txtusername.Text.Trim();
            newuser.PWD = txtuserpwd.Text;
            newuser.Author = (Authority)CBuserauthor.SelectedIndex;
            if (DialogResult.OK!=MessageBox.Show($"是否创建用户：{newuser.Name}","提示",MessageBoxButtons.OKCancel))
            {
                return;
            }
            if (!userservice.CheckEqualUser(newuser.Name))
            {
                userservice.UserAdd(newuser);
                SerializeHelper<User>.BinarySerialzeWrite("用户信息", userservice.Users);
                DataBinding();
            }
            else
            {
                MessageBox.Show("用户已经存在");
            }
           
           
        }
    }
}
