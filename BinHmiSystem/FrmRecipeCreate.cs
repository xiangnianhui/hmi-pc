﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UIDesgin.UIMessage;
using Models;
using UIDesgin.UIComponent;
using DAL;
using static UIDesgin.UIComponent.VarMesSend;

namespace BinHmiSystem
{
    public partial class FrmRecipeCreate : Form
    {
        //plc服务
      public  Plcservice plcservice = null;

        public FrmRecipeCreate()
        {
            InitializeComponent();
            DataInit();
            foreach (VarMesSend item in Controls.OfType<VarMesSend>())
            {
                item.ParamSetEvent+= new ParamSetDelegete(Var_paramSetEvent);
            }

        }
        //数据收集更新
        private void DataInit()
        {
            varValues.Clear();
            PlcValueDic.Clear();
            VarMsgDic.Clear();
            foreach (var item in this.Controls.OfType<VarMesSend>())
            {
                PlcVarValue varValue = new PlcVarValue();
                varValue.VarName = item.VarName;
                varValue.VarAddress = item.Address;
                varValue.Vartype = item.Type;
                varValue.VarValue = item.Value;
                varValues.Add(varValue);
                PlcValueDic.Add(item.Name, varValue);
                VarMsgDic.Add(item.Name, item);
            }
        }

        //字典1 记录控件名 和数据
        private Dictionary<string, PlcVarValue> PlcValueDic = new Dictionary<string, PlcVarValue>();
        //字典2 记录控件名 和控件
        private Dictionary<string, VarMesSend> VarMsgDic = new Dictionary<string, VarMesSend>();
        //list集合 记录plc变量信息
        private List<PlcVarValue> varValues = new List<PlcVarValue>();
        //窗口导入
        private void FrmRecipeCreate_Load(object sender, EventArgs e)
        {

        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK != MessageBox.Show("是否存储配方", "提示", MessageBoxButtons.OKCancel))
            {
                return;
            }
            string path = null;
            DataInit();
            if (MessageHelper.InputStringDialog(ref path, "输入配方名称", "配方保存"))
            {
                SerializeHelper<PlcVarValue>.XmlSerializeWriter(@"Recipe\" + path + ".xml", varValues);
                MessageBox.Show("sucess");
                return;
            }
            MessageBox.Show("fail");

        }
     //变量设置时间
        private void Var_paramSetEvent(object sender, EventArgs e)
        {
            //1.判断控件是否是自定义控件
            if (sender is VarMesSend tss)
            {
                if (PlcValueDic.ContainsKey(tss.Name))
                {
                    UIVarSetFrm frm = new UIVarSetFrm(plcservice, PlcValueDic[tss.Name], false);
                    frm.ShowDialog();
                    tss.Value = frm.Value.ToString();
                    DataInit();
                }
            }
        }
        //数据刷新
        private void btnPlcDataUpdate_Click(object sender, EventArgs e)
        {
            DataInit();
            ushort[] result;
            foreach (var value in PlcValueDic)
            {
                switch (value.Value.Vartype)
                {
                    case PlcDatatype.Bool:
                        break;
                    case PlcDatatype.Short:
                        break;
                    case PlcDatatype.Ushort:
                        result = plcservice.ReadHoldingRegisters(1, Convert.ToUInt16(value.Value.VarAddress), 1);
                        value.Value.VarValue = result[0];
                        VarMsgDic[value.Key].Value = result[0].ToString();
                        break;
                    case PlcDatatype.Int:
                        break;
                    case PlcDatatype.Uint:
                        break;
                    case PlcDatatype.Float:
                        break;
                    case PlcDatatype.Double:
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
