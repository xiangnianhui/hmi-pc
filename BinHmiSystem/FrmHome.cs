﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BinHmiSystem
{
    public partial class FrmHome : Form
    {
        public FrmHome()
        {
            InitializeComponent();
            int[] a = { 1, 2, 3, 4, 5, 6, 7, 8 };
            List<myvar> plcVars = new List<myvar>()
            { new myvar() { Name="A",Value=1},
             new myvar() { Name="B",Value=2},
              new myvar() { Name="C",Value=3},
               new myvar() { Name="D",Value=4},
             new myvar() { Name="E",Value=5}};
            chart1.DataSource = plcVars;
            //chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            chart1.ChartAreas[0].AxisY.Maximum = 100;
            chart1.ChartAreas[0].AxisY.Minimum = 0;
            chart1.Series[0].XValueMember = "Name";
            chart1.Series[0].YValueMembers = "Value";
        }
    }
    class myvar 
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
    
}
