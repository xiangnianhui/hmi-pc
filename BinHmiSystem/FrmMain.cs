﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using MetroFramework;
using MetroFramework.Forms;
using Models;
using Sunny.UI;

namespace BinHmiSystem
{
    public partial class FrmMain : Form
    {

        //plc控制器
        PlcController plc = null;
        //plc服务
        Plcservice Plcservice = null;
        //plc后台线程
        Thread plcTread;

        //窗体初始化
        public FrmMain()
        {
            InitializeComponent();
            LoginLoad();

        }
        //登陆检验
        private void LoginLoad()
        {
            this.Hide();
            FrmLogin frm = new FrmLogin();
            frm.ShowDialog();
            //判断是否登陆成功 如果登陆成功的话  用户不为空
            if (Userservice.Currentuser == null)
            {
                this.Close();
                Environment.Exit(0);
            }
            this.Show();
            labuser.Text = Userservice.Currentuser.Name;

        }

        //窗口界面类型
        private Type listForm;
        //界面切换
        private void FrmChange(Form myfrom)
        {

            if (listForm != myfrom.GetType())
            {              
                myfrom.TopLevel = false; //顶层窗口设为否
                myfrom.TopMost = false; //顶层窗口设为否
                myfrom.Parent = Panelcontent;//设置新窗体的父窗体
                myfrom.StartPosition = FormStartPosition.CenterParent;//新窗体开始位置
                myfrom.Dock = DockStyle.Fill;//新窗体分布方式 充满
                Panelcontent.Controls.Clear();//面板控件清除
                Panelcontent.Controls.Add(myfrom);  //面板控件加入
                myfrom.Show();//显示新窗体
                listForm = myfrom.GetType();//获取窗体的类型 便于下次判断
            }
        }

        #region 窗口移动
        private Point mouseOff;//鼠标移动位置变量
        private bool leftFlag;//鼠标是否为左键
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y);//获得当前鼠标的坐标
                leftFlag = true;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;//获得移动后鼠标的坐标
                mouseSet.Offset(mouseOff.X, mouseOff.Y);//设置移动后的位置
                Location = mouseSet;
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;
            }
        }
        #endregion

        //退出按钮
        private void BtnFClose_Click(object sender, EventArgs e)
        {
            //询问是否退出
            if (DialogResult.OK == MessageBox.Show("是否退出应用", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning))
            {
                this.Close();
                Environment.Exit(0);
            }
            else
            {
                return;
            }

        }
        //窗体加载
        private void FrmMain_Load(object sender, EventArgs e)
        {
            this.BtnStart.MouseMove += new MouseEventHandler(Btn_MouseMove);
            this.BtnStart.MouseLeave += new EventHandler(Btn_MouseLeave);
            this.BtnStop.MouseMove += new MouseEventHandler(Btn_MouseMove);
            this.BtnStop.MouseLeave += new EventHandler(Btn_MouseLeave);
            this.BtnHome.MouseMove += new MouseEventHandler(Btn_MouseMove);
            this.BtnHome.MouseLeave += new EventHandler(Btn_MouseLeave);
            FrmChange(new FrmHome());
            plcTread = new Thread(PlcDataThread);
            plcTread.IsBackground = true;
            plcTread.Start();

        }
        //plc数据更新线程
        private void PlcDataThread()
        {
            plc = new PlcController("192.168.0.10", 502, PlcType.西门子);
            Plcservice = new Plcservice(plc);
            try
            {
                while (true)
                {
                    if (!Plcservice.IsConnected)
                    {
                        try
                        {
                            Console.WriteLine("正在尝试连接");
                            Plcservice.OpenConnect();
                            if (Plcservice.IsConnected)
                            {
                                Console.WriteLine("连接成功");
                                MessageBox.Show("连接成功");
                                LedConnectState.bValue = Plcservice.IsConnected;

                            }

                        }

                        catch (Exception)
                        {
                            Console.WriteLine("连接失败");
                            Thread.Sleep(1000);
                            
                        }  
                    }
                    LedConnectState.bValue = Plcservice.IsConnected;
                    if (Plcservice != null && Plcservice.IsConnected)
                    {
                        Plcservice.ReadCoils(1, 1, 1);
                    }
                }

            }
            catch (Exception)
            {
                LedConnectState.bValue = Plcservice.IsConnect;
                Console.WriteLine("连接失败     重启连接");
                if (Plcservice!=null)
                {
                    Plcservice.DisConnecet();
                }
                MessageBox.Show("连接失败");
                Thread.Sleep(5000);
                PlcDataThread();
            }
        }
    
        //标签写入
        private void WriteIn_Tags(Control con)
            {
                foreach (Control item in con.Controls)
                {
                    string tags = item.Width.ToString() + "$";
                    tags += item.Height.ToString() + "$";
                    tags += item.Top.ToString() + "$";
                    tags += item.Left.ToString() + "$";
                    tags += item.Font.Size.ToString();
                    item.Tag = tags;
                    if (item.Controls.Count>0)
                    {
                        WriteIn_Tags(item);
                    }
                }
            }
        //重新定义控件大小
        private void Resize_Controls(Control con,double ScaleX,double ScaleY)
        {
            foreach (Control item in con.Controls)
            {
                var tags = (item.Tag.ToString()).Split(new char[]{'$'});
                int Awidth = Convert.ToInt32(tags[0]);
                int Aheight = Convert.ToInt32(tags[1]);
                int Atop = Convert.ToInt32(tags[2]);
                int Aleft = Convert.ToInt32(tags[3]);
               // int Afontsize = Convert.ToInt32(tags[4]);

                item.Width = Convert.ToInt32(Awidth * ScaleX);
                item.Height= Convert.ToInt32(Aheight * ScaleY);
                item.Left = Convert.ToInt32(Aleft * ScaleX);
                item.Top = Convert.ToInt32(Atop * ScaleY);
               // int fontsize = Convert.ToInt32(Afontsize * ScaleX);

                //con.Font = new Font(con.Font.Name,fontsize,con.Font.Style);
                if (con.Controls.Count>0)
                {
                    Resize_Controls(item, ScaleX, ScaleY);
                }
                    

            }
        }
        //鼠标进入
        private void Btn_MouseMove(object sender, MouseEventArgs e)
        {
            if (((UIImageButton)sender).Name == "BtnStart")
            {
                labstart.BackColor = Color.LightGreen;
            }
            if (((UIImageButton)sender).Name == "BtnStop")
            {
                labstop.BackColor = Color.LightGreen;
            }
            if (((UIImageButton)sender).Name == "BtnHome")
            {
                labhome.BackColor = Color.LightGreen;
            }

        }
        //鼠标离开
        private void Btn_MouseLeave(object sender, EventArgs e)
        {
            if (((UIImageButton)sender).Name == "BtnStart")
            {
                labstart.BackColor = Color.Transparent;
            }
            if (((UIImageButton)sender).Name == "BtnStop")
            {
                labstop.BackColor = Color.Transparent;
            }
            if (((UIImageButton)sender).Name == "BtnHome")
            {
                labhome.BackColor = Color.Transparent;
            }
        }
        //用户登陆界面
        private void 用户登陆ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoginLoad();
        }
        //切换至用户管理界面
        private void 用户管理ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmChange(new FrmUserManage());
        }
        //切换至配方创建界面
        private void 新建程序ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRecipeCreate frm = new FrmRecipeCreate();
            frm.plcservice = this.Plcservice;
            FrmChange(frm);
        }
        //切换至配方下载界面
        private void 下载配方ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRecipeLoad frm = new FrmRecipeLoad();
            frm.plcservice = this.Plcservice;
            
            FrmChange(frm);
        }
        //切换至实时报警界面
        private void 实时报警ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAlarmView frm = new FrmAlarmView();
            frm.plcservice = this.Plcservice;

            FrmChange(frm);
        }
        //切换至历史报警界面
        private void 历史报警ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmHistroyAlarm frm = new FrmHistroyAlarm();
            FrmChange(frm);
        }
        //切换至报警设置界面
        private void 报警设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAlarmEdit frm = new FrmAlarmEdit();
            FrmChange(frm);
        }
        //切换至主页界面
        private void BtnHome_Click(object sender, EventArgs e)
        {
            FrmChange(new FrmHome());
            LogHelper.AddLog("点击勒home键盘");
        }
        //窗口最大化
        private void BtnWindowsMax_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }
        //注销登陆
        private void BtnLoginOut_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否注销当前用户","提示",MessageBoxButtons.OKCancel,MessageBoxIcon.Information)!=DialogResult.OK)
            {
                return;
            }
            Userservice.Currentuser = null;
            labuser.Text = "null";
        }

      
    }
}
