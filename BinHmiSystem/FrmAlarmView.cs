﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using Models;
using Sunny.UI;

namespace BinHmiSystem
{
    public partial class FrmAlarmView : Form
    {
        //plc服务设备
        public Plcservice plcservice;
        //报警字典
        Dictionary<bool, string> AlarmList = new Dictionary<bool, string>();

        //报警集合
        List<PlcAlarmCoil> alarmVars = new List<PlcAlarmCoil>();
        //发生报警的变量
        List<PlcAlarmCoil> OccurAlarmVar = new List<PlcAlarmCoil>();
        //报警刷新定时器
        System.Timers.Timer alramflushtimer;

        List<UITextBox> uITexts = new List<UITextBox>();
        bool[] data = new bool[10] { false, false, false, false, false, false, true, false, false, false };
        public FrmAlarmView()
        {
            InitializeComponent();
            FrmInit();//报警初始化
            alramflushtimer = new System.Timers.Timer();//新增后台报警信息刷新时钟
            alramflushtimer.Enabled = true;//允许时钟计时
            alramflushtimer.Interval = 250;//刷新间隔位250ms
            alramflushtimer.Elapsed += new System.Timers.ElapsedEventHandler(AlarmFlushTime);//更新刷新时间
            alramflushtimer.Start();//及时开始
        }

        private void BtnInfoUpdate_Click(object sender, EventArgs e)
        {
            FrmInit();
        }

        //报警刷新定时内容
        private void AlarmFlushTime(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (plcservice != null && plcservice.IsConnected)
            {
                try
                {
                    //foreach (var item in alarmVars)
                    //{
                    //    item.VarValue = plcservice.ReadHoldingRegisters(1, ushort.Parse(item.VarAddress), 1)[0];
                    //}


                }
                catch (Exception E)
                {
                    Console.WriteLine("读取数据错误    " + E.Message);
                    return;
                }
            }

            if (listAlarm.InvokeRequired)
            {
                listAlarm.BeginInvoke(new Action(() =>
                {
                    int SerialNum = 0;
                    listAlarm.Items.Clear();
                    OccurAlarmVar = PlcAlarmService.CheckCoilState(ref alarmVars);
                    #region 旧的报警程序                  
                    //foreach (var item in alarmVars)
                    //{
                    //    if (item.regvalue)
                    //    {
                    //        if (item.ocreenceTime <= time)
                    //        {
                    //            item.ocreenceTime = DateTime.Now;
                    //        }
                    //       // listAlarm.Items.Add(item.ocreenceTime.ToString() + "   " + item.alarmInfo);
                    //    }
                    //    else
                    //    {
                    //        item.ocreenceTime = time;
                    //    }
                    //}
                    ////----------------------------------
                    //List<AlarmVar> alarmsort = new List<AlarmVar>();
                    //foreach (var item in alarmVars)
                    //{
                    //    alarmsort.Add(item);
                    //}
                    //alarmsort.Sort();
                    ////--------------------------------
                    //int SerialNum = 0;
                    //foreach (var item in alarmsort)
                    //{
                    //    if (item.regvalue)
                    //    {
                    //        ++SerialNum;
                    //        listAlarm.Items.Add("  "+SerialNum.ToString() +"    "+item.ocreenceTime.ToString() + "   " + item.alarmInfo);
                    //    }
                    // }
                    #endregion
                    foreach (var item in OccurAlarmVar)
                    {
                        if (Convert.ToBoolean(item.VarValue))
                        {
                            ++SerialNum;
                            listAlarm.Items.Add("  " + SerialNum.ToString() + "    " + item.OccurrentTime.ToString() + "   " + item.AlarmInfo);
                        }
                    }
                }));
            }

        }
        //初始化
        private void FrmInit()
        {
            alarmVars = SerializeHelper<PlcAlarmCoil>.XmlSerializeRead(PlcAlarmService.AlarmListPath);
            foreach (UITextBox item in this.Controls.OfType<UITextBox>())
            {
                uITexts.Add(item);
            }
            timer1.Start();

        }


        //删除选中报警信息
        private void BtnDel_Click(object sender, EventArgs e)
        {
            if (listAlarm.SelectedIndex < 0)
            {
                return;
            }
            int selectindex = listAlarm.SelectedIndex;
            listAlarm.Items.RemoveAt(selectindex);
        }

        private void FrmAlarmView_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (alramflushtimer.Enabled == true)
            {
                alramflushtimer.Dispose();
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        plcservice.WriteMultipleCoils(1, 0, data);

            foreach (var item in uITexts)
            {
                item.Text = data[Convert.ToInt32(item.Tag)].ToString();
            }
        }
    }
}
