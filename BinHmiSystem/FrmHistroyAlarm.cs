﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using Models;

namespace BinHmiSystem
{  
    public partial class FrmHistroyAlarm : Form
    {
        public List<PlcAlarmCoil> alarmCoils=new List<PlcAlarmCoil>();
        public FrmHistroyAlarm()
        {
            InitializeComponent();
            for (int i = 0; i < 10; i++)
            {
                alarmCoils.Add(new PlcAlarmCoil() { VarAddress = i.ToString() }) ;
            }
           
            
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            SerializeHelper<PlcAlarmCoil>.XmlSerializeWriter("alarmtest.xml", alarmCoils);
        }
    }
}
