﻿namespace BinHmiSystem
{
    partial class FrmRecipeCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnSave = new Sunny.UI.UISymbolButton();
            this.Var2 = new UIDesgin.UIComponent.VarMesSend();
            this.Var3 = new UIDesgin.UIComponent.VarMesSend();
            this.Var1 = new UIDesgin.UIComponent.VarMesSend();
            this.integerNumUpDown8 = new UIDesgin.UIComponent.IntegerNumUpDown();
            this.integerNumUpDown7 = new UIDesgin.UIComponent.IntegerNumUpDown();
            this.integerNumUpDown6 = new UIDesgin.UIComponent.IntegerNumUpDown();
            this.integerNumUpDown5 = new UIDesgin.UIComponent.IntegerNumUpDown();
            this.integerNumUpDown4 = new UIDesgin.UIComponent.IntegerNumUpDown();
            this.integerNumUpDown3 = new UIDesgin.UIComponent.IntegerNumUpDown();
            this.integerNumUpDown2 = new UIDesgin.UIComponent.IntegerNumUpDown();
            this.integerNumUpDown1 = new UIDesgin.UIComponent.IntegerNumUpDown();
            this.varMesSend1 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend2 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend3 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend4 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend5 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend6 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend7 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend8 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend9 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend10 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend11 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend12 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend13 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend14 = new UIDesgin.UIComponent.VarMesSend();
            this.varMesSend15 = new UIDesgin.UIComponent.VarMesSend();
            this.btnPlcDataUpdate = new Sunny.UI.UIImageButton();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btnPlcDataUpdate)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnSave
            // 
            this.BtnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnSave.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.BtnSave.Location = new System.Drawing.Point(1067, 621);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(133, 40);
            this.BtnSave.Symbol = 61563;
            this.BtnSave.TabIndex = 39;
            this.BtnSave.Text = "保存配方";
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // Var2
            // 
            this.Var2.Address = "251";
            this.Var2.BackColor = System.Drawing.Color.Transparent;
            this.Var2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Var2.Location = new System.Drawing.Point(249, 125);
            this.Var2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Var2.Name = "Var2";
            this.Var2.Size = new System.Drawing.Size(338, 40);
            this.Var2.TabIndex = 37;
            this.Var2.Type = Models.PlcDatatype.Ushort;
            this.Var2.Value = "0";
            this.Var2.VarName = "生产数量";
            this.Var2.VarUnit = "个";
            // 
            // Var3
            // 
            this.Var3.Address = "252";
            this.Var3.BackColor = System.Drawing.Color.Transparent;
            this.Var3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Var3.Location = new System.Drawing.Point(249, 183);
            this.Var3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Var3.Name = "Var3";
            this.Var3.Size = new System.Drawing.Size(338, 40);
            this.Var3.TabIndex = 36;
            this.Var3.Type = Models.PlcDatatype.Ushort;
            this.Var3.Value = "0";
            this.Var3.VarName = "剩余数量";
            this.Var3.VarUnit = "个";
            // 
            // Var1
            // 
            this.Var1.Address = "250";
            this.Var1.BackColor = System.Drawing.Color.Transparent;
            this.Var1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Var1.Location = new System.Drawing.Point(249, 67);
            this.Var1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Var1.Name = "Var1";
            this.Var1.Size = new System.Drawing.Size(338, 40);
            this.Var1.TabIndex = 35;
            this.Var1.Type = Models.PlcDatatype.Ushort;
            this.Var1.Value = "0";
            this.Var1.VarName = "计划数量";
            this.Var1.VarUnit = "个";
            // 
            // integerNumUpDown8
            // 
            this.integerNumUpDown8.BackColor = System.Drawing.Color.White;
            this.integerNumUpDown8.Location = new System.Drawing.Point(33, 473);
            this.integerNumUpDown8.MaxValue = 999999;
            this.integerNumUpDown8.Name = "integerNumUpDown8";
            this.integerNumUpDown8.Size = new System.Drawing.Size(159, 33);
            this.integerNumUpDown8.TabIndex = 33;
            this.integerNumUpDown8.Type = Models.PlcDatatype.Ushort;
            // 
            // integerNumUpDown7
            // 
            this.integerNumUpDown7.BackColor = System.Drawing.Color.White;
            this.integerNumUpDown7.Location = new System.Drawing.Point(33, 415);
            this.integerNumUpDown7.MaxValue = 999999;
            this.integerNumUpDown7.Name = "integerNumUpDown7";
            this.integerNumUpDown7.Size = new System.Drawing.Size(159, 33);
            this.integerNumUpDown7.TabIndex = 32;
            this.integerNumUpDown7.Type = Models.PlcDatatype.Ushort;
            // 
            // integerNumUpDown6
            // 
            this.integerNumUpDown6.BackColor = System.Drawing.Color.White;
            this.integerNumUpDown6.Location = new System.Drawing.Point(33, 357);
            this.integerNumUpDown6.MaxValue = 999999;
            this.integerNumUpDown6.Name = "integerNumUpDown6";
            this.integerNumUpDown6.Size = new System.Drawing.Size(159, 33);
            this.integerNumUpDown6.TabIndex = 31;
            this.integerNumUpDown6.Type = Models.PlcDatatype.Ushort;
            // 
            // integerNumUpDown5
            // 
            this.integerNumUpDown5.BackColor = System.Drawing.Color.White;
            this.integerNumUpDown5.Location = new System.Drawing.Point(33, 299);
            this.integerNumUpDown5.MaxValue = 999999;
            this.integerNumUpDown5.Name = "integerNumUpDown5";
            this.integerNumUpDown5.Size = new System.Drawing.Size(159, 33);
            this.integerNumUpDown5.TabIndex = 30;
            this.integerNumUpDown5.Type = Models.PlcDatatype.Ushort;
            // 
            // integerNumUpDown4
            // 
            this.integerNumUpDown4.BackColor = System.Drawing.Color.White;
            this.integerNumUpDown4.Location = new System.Drawing.Point(33, 241);
            this.integerNumUpDown4.MaxValue = 999999;
            this.integerNumUpDown4.Name = "integerNumUpDown4";
            this.integerNumUpDown4.Size = new System.Drawing.Size(159, 33);
            this.integerNumUpDown4.TabIndex = 29;
            this.integerNumUpDown4.Type = Models.PlcDatatype.Ushort;
            // 
            // integerNumUpDown3
            // 
            this.integerNumUpDown3.BackColor = System.Drawing.Color.White;
            this.integerNumUpDown3.Location = new System.Drawing.Point(33, 183);
            this.integerNumUpDown3.MaxValue = 999999;
            this.integerNumUpDown3.Name = "integerNumUpDown3";
            this.integerNumUpDown3.Size = new System.Drawing.Size(159, 33);
            this.integerNumUpDown3.TabIndex = 28;
            this.integerNumUpDown3.Type = Models.PlcDatatype.Ushort;
            // 
            // integerNumUpDown2
            // 
            this.integerNumUpDown2.BackColor = System.Drawing.Color.White;
            this.integerNumUpDown2.Location = new System.Drawing.Point(33, 125);
            this.integerNumUpDown2.MaxValue = 999999;
            this.integerNumUpDown2.Name = "integerNumUpDown2";
            this.integerNumUpDown2.Size = new System.Drawing.Size(159, 33);
            this.integerNumUpDown2.TabIndex = 27;
            this.integerNumUpDown2.Type = Models.PlcDatatype.Ushort;
            // 
            // integerNumUpDown1
            // 
            this.integerNumUpDown1.BackColor = System.Drawing.Color.White;
            this.integerNumUpDown1.Location = new System.Drawing.Point(33, 68);
            this.integerNumUpDown1.MaxValue = 1000;
            this.integerNumUpDown1.Name = "integerNumUpDown1";
            this.integerNumUpDown1.Size = new System.Drawing.Size(162, 32);
            this.integerNumUpDown1.Step = 2;
            this.integerNumUpDown1.TabIndex = 26;
            this.integerNumUpDown1.Type = Models.PlcDatatype.Ushort;
            this.integerNumUpDown1.Value = 102;
            // 
            // varMesSend1
            // 
            this.varMesSend1.Address = "254";
            this.varMesSend1.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend1.Location = new System.Drawing.Point(249, 299);
            this.varMesSend1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend1.Name = "varMesSend1";
            this.varMesSend1.Size = new System.Drawing.Size(338, 40);
            this.varMesSend1.TabIndex = 42;
            this.varMesSend1.Type = Models.PlcDatatype.Ushort;
            this.varMesSend1.Value = "0";
            this.varMesSend1.VarName = "2";
            this.varMesSend1.VarUnit = "个";
            // 
            // varMesSend2
            // 
            this.varMesSend2.Address = "255";
            this.varMesSend2.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend2.Location = new System.Drawing.Point(249, 357);
            this.varMesSend2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend2.Name = "varMesSend2";
            this.varMesSend2.Size = new System.Drawing.Size(338, 40);
            this.varMesSend2.TabIndex = 41;
            this.varMesSend2.Type = Models.PlcDatatype.Ushort;
            this.varMesSend2.Value = "0";
            this.varMesSend2.VarName = "3";
            this.varMesSend2.VarUnit = "个";
            // 
            // varMesSend3
            // 
            this.varMesSend3.Address = "253";
            this.varMesSend3.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend3.Location = new System.Drawing.Point(249, 241);
            this.varMesSend3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend3.Name = "varMesSend3";
            this.varMesSend3.Size = new System.Drawing.Size(338, 40);
            this.varMesSend3.TabIndex = 40;
            this.varMesSend3.Type = Models.PlcDatatype.Ushort;
            this.varMesSend3.Value = "0";
            this.varMesSend3.VarName = "1";
            this.varMesSend3.VarUnit = "个";
            // 
            // varMesSend4
            // 
            this.varMesSend4.Address = "257";
            this.varMesSend4.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend4.Location = new System.Drawing.Point(249, 473);
            this.varMesSend4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend4.Name = "varMesSend4";
            this.varMesSend4.Size = new System.Drawing.Size(338, 40);
            this.varMesSend4.TabIndex = 45;
            this.varMesSend4.Type = Models.PlcDatatype.Ushort;
            this.varMesSend4.Value = "0";
            this.varMesSend4.VarName = "5";
            this.varMesSend4.VarUnit = "个";
            // 
            // varMesSend5
            // 
            this.varMesSend5.Address = "258";
            this.varMesSend5.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend5.Location = new System.Drawing.Point(249, 531);
            this.varMesSend5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend5.Name = "varMesSend5";
            this.varMesSend5.Size = new System.Drawing.Size(338, 40);
            this.varMesSend5.TabIndex = 44;
            this.varMesSend5.Type = Models.PlcDatatype.Ushort;
            this.varMesSend5.Value = "0";
            this.varMesSend5.VarName = "6";
            this.varMesSend5.VarUnit = "个";
            // 
            // varMesSend6
            // 
            this.varMesSend6.Address = "256";
            this.varMesSend6.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend6.Location = new System.Drawing.Point(249, 415);
            this.varMesSend6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend6.Name = "varMesSend6";
            this.varMesSend6.Size = new System.Drawing.Size(338, 40);
            this.varMesSend6.TabIndex = 43;
            this.varMesSend6.Type = Models.PlcDatatype.Ushort;
            this.varMesSend6.Value = "0";
            this.varMesSend6.VarName = "4";
            this.varMesSend6.VarUnit = "个";
            // 
            // varMesSend7
            // 
            this.varMesSend7.Address = "266";
            this.varMesSend7.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend7.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend7.Location = new System.Drawing.Point(664, 474);
            this.varMesSend7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend7.Name = "varMesSend7";
            this.varMesSend7.Size = new System.Drawing.Size(338, 40);
            this.varMesSend7.TabIndex = 54;
            this.varMesSend7.Type = Models.PlcDatatype.Ushort;
            this.varMesSend7.Value = "0";
            this.varMesSend7.VarName = "14";
            this.varMesSend7.VarUnit = "个";
            // 
            // varMesSend8
            // 
            this.varMesSend8.Address = "267";
            this.varMesSend8.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend8.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend8.Location = new System.Drawing.Point(664, 532);
            this.varMesSend8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend8.Name = "varMesSend8";
            this.varMesSend8.Size = new System.Drawing.Size(338, 40);
            this.varMesSend8.TabIndex = 53;
            this.varMesSend8.Type = Models.PlcDatatype.Ushort;
            this.varMesSend8.Value = "0";
            this.varMesSend8.VarName = "15";
            this.varMesSend8.VarUnit = "个";
            // 
            // varMesSend9
            // 
            this.varMesSend9.Address = "265";
            this.varMesSend9.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend9.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend9.Location = new System.Drawing.Point(664, 416);
            this.varMesSend9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend9.Name = "varMesSend9";
            this.varMesSend9.Size = new System.Drawing.Size(338, 40);
            this.varMesSend9.TabIndex = 52;
            this.varMesSend9.Type = Models.PlcDatatype.Ushort;
            this.varMesSend9.Value = "0";
            this.varMesSend9.VarName = "13";
            this.varMesSend9.VarUnit = "个";
            // 
            // varMesSend10
            // 
            this.varMesSend10.Address = "263";
            this.varMesSend10.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend10.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend10.Location = new System.Drawing.Point(664, 300);
            this.varMesSend10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend10.Name = "varMesSend10";
            this.varMesSend10.Size = new System.Drawing.Size(338, 40);
            this.varMesSend10.TabIndex = 51;
            this.varMesSend10.Type = Models.PlcDatatype.Ushort;
            this.varMesSend10.Value = "0";
            this.varMesSend10.VarName = "11";
            this.varMesSend10.VarUnit = "个";
            // 
            // varMesSend11
            // 
            this.varMesSend11.Address = "264";
            this.varMesSend11.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend11.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend11.Location = new System.Drawing.Point(664, 358);
            this.varMesSend11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend11.Name = "varMesSend11";
            this.varMesSend11.Size = new System.Drawing.Size(338, 40);
            this.varMesSend11.TabIndex = 50;
            this.varMesSend11.Type = Models.PlcDatatype.Ushort;
            this.varMesSend11.Value = "0";
            this.varMesSend11.VarName = "12";
            this.varMesSend11.VarUnit = "个";
            // 
            // varMesSend12
            // 
            this.varMesSend12.Address = "262";
            this.varMesSend12.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend12.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend12.Location = new System.Drawing.Point(664, 242);
            this.varMesSend12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend12.Name = "varMesSend12";
            this.varMesSend12.Size = new System.Drawing.Size(338, 40);
            this.varMesSend12.TabIndex = 49;
            this.varMesSend12.Type = Models.PlcDatatype.Ushort;
            this.varMesSend12.Value = "0";
            this.varMesSend12.VarName = "10";
            this.varMesSend12.VarUnit = "个";
            // 
            // varMesSend13
            // 
            this.varMesSend13.Address = "260";
            this.varMesSend13.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend13.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend13.Location = new System.Drawing.Point(664, 126);
            this.varMesSend13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend13.Name = "varMesSend13";
            this.varMesSend13.Size = new System.Drawing.Size(338, 40);
            this.varMesSend13.TabIndex = 48;
            this.varMesSend13.Type = Models.PlcDatatype.Ushort;
            this.varMesSend13.Value = "0";
            this.varMesSend13.VarName = "8";
            this.varMesSend13.VarUnit = "个";
            // 
            // varMesSend14
            // 
            this.varMesSend14.Address = "261";
            this.varMesSend14.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend14.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend14.Location = new System.Drawing.Point(664, 184);
            this.varMesSend14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend14.Name = "varMesSend14";
            this.varMesSend14.Size = new System.Drawing.Size(338, 40);
            this.varMesSend14.TabIndex = 47;
            this.varMesSend14.Type = Models.PlcDatatype.Ushort;
            this.varMesSend14.Value = "0";
            this.varMesSend14.VarName = "9";
            this.varMesSend14.VarUnit = "个";
            // 
            // varMesSend15
            // 
            this.varMesSend15.Address = "259";
            this.varMesSend15.BackColor = System.Drawing.Color.Transparent;
            this.varMesSend15.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.varMesSend15.Location = new System.Drawing.Point(664, 68);
            this.varMesSend15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.varMesSend15.Name = "varMesSend15";
            this.varMesSend15.Size = new System.Drawing.Size(338, 40);
            this.varMesSend15.TabIndex = 46;
            this.varMesSend15.Type = Models.PlcDatatype.Ushort;
            this.varMesSend15.Value = "0";
            this.varMesSend15.VarName = "7";
            this.varMesSend15.VarUnit = "个";
            // 
            // btnPlcDataUpdate
            // 
            this.btnPlcDataUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPlcDataUpdate.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.btnPlcDataUpdate.Image = global::BinHmiSystem.Properties.Resources.同步1;
            this.btnPlcDataUpdate.ImagePress = global::BinHmiSystem.Properties.Resources.同步2;
            this.btnPlcDataUpdate.Location = new System.Drawing.Point(1147, 8);
            this.btnPlcDataUpdate.Name = "btnPlcDataUpdate";
            this.btnPlcDataUpdate.Size = new System.Drawing.Size(40, 38);
            this.btnPlcDataUpdate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnPlcDataUpdate.TabIndex = 57;
            this.btnPlcDataUpdate.TabStop = false;
            this.btnPlcDataUpdate.Text = null;
            this.btnPlcDataUpdate.Click += new System.EventHandler(this.btnPlcDataUpdate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1139, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 15);
            this.label1.TabIndex = 58;
            this.label1.Text = "同步PLC";
            // 
            // FrmRecipeCreate
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(1212, 673);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPlcDataUpdate);
            this.Controls.Add(this.varMesSend7);
            this.Controls.Add(this.varMesSend8);
            this.Controls.Add(this.varMesSend9);
            this.Controls.Add(this.varMesSend10);
            this.Controls.Add(this.varMesSend11);
            this.Controls.Add(this.varMesSend12);
            this.Controls.Add(this.varMesSend13);
            this.Controls.Add(this.varMesSend14);
            this.Controls.Add(this.varMesSend15);
            this.Controls.Add(this.varMesSend4);
            this.Controls.Add(this.varMesSend5);
            this.Controls.Add(this.varMesSend6);
            this.Controls.Add(this.varMesSend1);
            this.Controls.Add(this.varMesSend2);
            this.Controls.Add(this.varMesSend3);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.Var2);
            this.Controls.Add(this.Var3);
            this.Controls.Add(this.Var1);
            this.Controls.Add(this.integerNumUpDown8);
            this.Controls.Add(this.integerNumUpDown7);
            this.Controls.Add(this.integerNumUpDown6);
            this.Controls.Add(this.integerNumUpDown5);
            this.Controls.Add(this.integerNumUpDown4);
            this.Controls.Add(this.integerNumUpDown3);
            this.Controls.Add(this.integerNumUpDown2);
            this.Controls.Add(this.integerNumUpDown1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmRecipeCreate";
            this.Text = "FrmRecipeCreate";
            this.Load += new System.EventHandler(this.FrmRecipeCreate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btnPlcDataUpdate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private UIDesgin.UIComponent.IntegerNumUpDown integerNumUpDown1;
        private UIDesgin.UIComponent.IntegerNumUpDown integerNumUpDown2;
        private UIDesgin.UIComponent.IntegerNumUpDown integerNumUpDown3;
        private UIDesgin.UIComponent.IntegerNumUpDown integerNumUpDown4;
        private UIDesgin.UIComponent.IntegerNumUpDown integerNumUpDown5;
        private UIDesgin.UIComponent.IntegerNumUpDown integerNumUpDown6;
        private UIDesgin.UIComponent.IntegerNumUpDown integerNumUpDown7;
        private UIDesgin.UIComponent.IntegerNumUpDown integerNumUpDown8;
        private UIDesgin.UIComponent.VarMesSend Var1;
        private UIDesgin.UIComponent.VarMesSend Var3;
        private UIDesgin.UIComponent.VarMesSend Var2;
        private Sunny.UI.UISymbolButton BtnSave;
        private UIDesgin.UIComponent.VarMesSend varMesSend1;
        private UIDesgin.UIComponent.VarMesSend varMesSend2;
        private UIDesgin.UIComponent.VarMesSend varMesSend3;
        private UIDesgin.UIComponent.VarMesSend varMesSend4;
        private UIDesgin.UIComponent.VarMesSend varMesSend5;
        private UIDesgin.UIComponent.VarMesSend varMesSend6;
        private UIDesgin.UIComponent.VarMesSend varMesSend7;
        private UIDesgin.UIComponent.VarMesSend varMesSend8;
        private UIDesgin.UIComponent.VarMesSend varMesSend9;
        private UIDesgin.UIComponent.VarMesSend varMesSend10;
        private UIDesgin.UIComponent.VarMesSend varMesSend11;
        private UIDesgin.UIComponent.VarMesSend varMesSend12;
        private UIDesgin.UIComponent.VarMesSend varMesSend13;
        private UIDesgin.UIComponent.VarMesSend varMesSend14;
        private UIDesgin.UIComponent.VarMesSend varMesSend15;
        private Sunny.UI.UIImageButton btnPlcDataUpdate;
        private System.Windows.Forms.Label label1;
    }
}