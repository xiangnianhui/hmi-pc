﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Models
{
    /// <summary>
    /// 用户权限枚举
    /// </summary>
    public enum Authority
    {
        OP,
        ME,
        EE,
        SA
    }

    /// <summary>
    /// 用户类
    /// </summary>
    [Serializable]
    public class User
    {
        //用户编号
        public int ID
        {
            get; set;
        }
        //用户名
        [XmlAttribute(AttributeName = "Name")]
        public string Name
        {
            get;
            set;
        }
        //用户密码
        public string PWD
        {
            get;
            set;
        }
        public Authority Author
        {
            get;
            set;
        }

        //构造函数
        public User(string name,string pwd,Authority author)
        {
            this.Name = name;
            this.PWD = pwd;
            this.Author = author;
        }
        public User()
        {

        }
       
    }
}
