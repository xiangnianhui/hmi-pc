﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
   public class FileS
    {
        /// <summary>
        /// 文件名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 大小
        /// </summary>
        public  int Size { get; set; }
    }
}
