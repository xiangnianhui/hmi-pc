﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;


namespace Models
{
    /// <summary>
    /// PLC数据类型
    /// </summary>
   public enum PlcDatatype
    {
        Bool,
        Short,
        Ushort,
        Int,
        Uint,
        Float,
        Double
    }
    [Serializable]
    public class PlcVarValue
    {
        /// <summary>
        /// 变量名称
        /// </summary>
        public string  VarName { get; set; }
        /// <summary>
        /// 变量地址
        /// </summary>
        [XmlAttribute(AttributeName = "VarAddress")]
        public string VarAddress { get; set; }
        /// <summary>
        /// 变量类型
        /// </summary>
        public PlcDatatype Vartype { get; set; }
        /// <summary>
        /// 变量值
        /// </summary>
        public object VarValue { get; set; } = 0;

      
    }
}
