﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public enum PlcType
    {
        西门子,
        三菱,
        欧姆龙
        
    }

    public enum Bytebyte
    {
        ABCD,//1234
        CDAB,//3412
        BADC,//2143
        DCBA//4321
    }

    public  class PlcController
    {
        #region  属性

        /// <summary>
        /// plc名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// plc通讯地址
        /// </summary>
        public string Ip{ get;set;}

        /// <summary>
        /// 端口号
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// 变量
        /// </summary>
        public List<PlcVarValue> plcVarValues;

        /// <summary>
        /// plc字节序
        /// </summary>
        public Bytebyte bytebyte;

       
        private PlcType plctype;
        /// <summary>
        /// plc品牌类型
        /// </summary>
        public PlcType Plctype
        {
            get { return plctype; }
            set 
            {
                plctype = value;
                switch (value)
                {
                    case PlcType.西门子:
                        bytebyte = Bytebyte.CDAB;
                        break;
                    case PlcType.三菱:
                        bytebyte = Bytebyte.ABCD;
                        break;
                    case PlcType.欧姆龙:
                        bytebyte = Bytebyte.ABCD;
                        break;
                    default:
                        bytebyte = Bytebyte.CDAB;
                        break;
                }
            }
        }

     
        #endregion

        public PlcController(string ip,int port,PlcType type)
        {
            this.Ip = ip;
            this.Port = port;
            this.Plctype = type;
            plcVarValues = new List<PlcVarValue>();
        }


       

    }
}
