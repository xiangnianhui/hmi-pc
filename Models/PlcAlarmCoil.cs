﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    ///PlcCoil报警
    /// </summary>
    [Serializable]
    public class PlcAlarmCoil : PlcVarValue,IComparable 
    {

        #region   属性
        /// <summary>
        /// 报警信息
        /// </summary>
        public string AlarmInfo { get; set; } = "null";
        /// <summary>
        /// 报警发生时间
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public DateTime OccurrentTime { get; set; }
        #endregion


        #region 方法
        /// <summary>
        /// 按照时间进行排序的比较方法
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            if (this.OccurrentTime > ((PlcAlarmCoil)obj).OccurrentTime)
            {
                return 1;
            }
            else if (this.OccurrentTime == ((PlcAlarmCoil)obj).OccurrentTime)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        #endregion


    }
}
